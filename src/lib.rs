use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub fn input_file_to_vec_u32(filename: &str) -> Result<Vec<u32>, Box<dyn Error>> {
    // Open file.
    let path = Path::new(filename);
    let file = File::open(&path)?;

    // Read each line into the vector.
    let mut inputs = Vec::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(num) = line {
            inputs.push(num.parse()?);
        }
    }

    Ok(inputs)
}

/// Convert a file with a single line of comma seperated values into a vector of generic T.
pub fn file_single_line_to_vec<T>(filename: &str) -> Result<Vec<T>, Box<dyn Error>>
where
    T: std::str::FromStr,
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    let path = Path::new(filename);
    let file = File::open(&path)?;
    let mut reader = io::BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line)?;

    Ok(line
        .trim()
        .split(",")
        .map(|x| x.parse::<T>().unwrap())
        .collect::<Vec<T>>())
}

/// Convert a file into a vector of strings. One for each line.
pub fn input_file(filename: &str) -> Result<Vec<String>, Box<dyn Error>> {
    let path = Path::new(filename);
    let file = File::open(&path)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}
