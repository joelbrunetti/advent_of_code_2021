//! --- Day 24: Arithmetic Logic Unit ---
//!
//! Magic smoke starts leaking from the submarine's arithmetic logic unit (ALU). Without the ability to perform basic arithmetic and logic functions, the submarine can't produce cool patterns with its Christmas lights!
//!
//! It also can't navigate. Or run the oxygen system.
//!
//! Don't worry, though - you probably have enough oxygen left to give you enough time to build a new ALU.
//!
//! The ALU is a four-dimensional processing unit: it has integer variables w, x, y, and z. These variables all start with the value 0. The ALU also supports six instructions:
//!
//!     inp a - Read an input value and write it to variable a.
//!     add a b - Add the value of a to the value of b, then store the result in variable a.
//!     mul a b - Multiply the value of a by the value of b, then store the result in variable a.
//!     div a b - Divide the value of a by the value of b, truncate the result to an integer, then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
//!     mod a b - Divide the value of a by the value of b, then store the remainder in variable a. (This is also called the modulo operation.)
//!     eql a b - If the value of a and b are equal, then store the value 1 in variable a. Otherwise, store the value 0 in variable a.
//!
//! In all of these instructions, a and b are placeholders; a will always be the variable where the result of the operation is stored (one of w, x, y, or z), while b can be either a variable or a number. Numbers can be positive or negative, but will always be integers.
//!
//! The ALU has no jump instructions; in an ALU program, every instruction is run exactly once in order from top to bottom. The program halts after the last instruction has finished executing.
//!
//! (Program authors should be especially cautious; attempting to execute div with b=0 or attempting to execute mod with a<0 or b<=0 will cause the program to crash and might even damage the ALU. These operations are never intended in any serious ALU program.)
//!
//! For example, here is an ALU program which takes an input number, negates it, and stores it in x:
//!
//! inp x
//! mul x -1
//!
//! Here is an ALU program which takes two input numbers, then sets z to 1 if the second input number is three times larger than the first input number, or sets z to 0 otherwise:
//!
//! inp z
//! inp x
//! mul z 3
//! eql z x
//!
//! Here is an ALU program which takes a non-negative integer as input, converts it into binary, and stores the lowest (1's) bit in z, the second-lowest (2's) bit in y, the third-lowest (4's) bit in x, and the fourth-lowest (8's) bit in w:
//!
//! inp w
//! add z w
//! mod z 2
//! div w 2
//! add y w
//! mod y 2
//! div w 2
//! add x w
//! mod x 2
//! div w 2
//! mod w 2
//!
//! Once you have built a replacement ALU, you can install it in the submarine, which will immediately resume what it was doing when the ALU failed: validating the submarine's model number. To do this, the ALU will run the MOdel Number Automatic Detector program (MONAD, your puzzle input).
//!
//! Submarine model numbers are always fourteen-digit numbers consisting only of digits 1 through 9. The digit 0 cannot appear in a model number.
//!
//! When MONAD checks a hypothetical fourteen-digit model number, it uses fourteen separate inp instructions, each expecting a single digit of the model number in order of most to least significant. (So, to check the model number 13579246899999, you would give 1 to the first inp instruction, 3 to the second inp instruction, 5 to the third inp instruction, and so on.) This means that when operating MONAD, each input instruction should only ever be given an integer value of at least 1 and at most 9.
//!
//! Then, after MONAD has finished running all of its instructions, it will indicate that the model number was valid by leaving a 0 in variable z. However, if the model number was invalid, it will leave some other non-zero value in z.
//!
//! MONAD imposes additional, mysterious restrictions on model numbers, and legend says the last copy of the MONAD documentation was eaten by a tanuki. You'll need to figure out what MONAD does some other way.
//!
//! To enable as many submarine features as possible, find the largest valid fourteen-digit model number that contains no 0 digits. What is the largest model number accepted by MONAD?
//!
//!--- Part Two ---
//!
//! As the submarine starts booting up things like the Retro Encabulator, you realize that maybe you don't need all these submarine features after all.
//!
//! What is the smallest model number accepted by MONAD?
//!

use std::collections::VecDeque;
use std::thread;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day23.input").unwrap();
    let inst: Vec<Operation> = lines.iter().map(|s| Operation::from_str(s)).collect();

    let up = true;
    let min: i64 = 11_111_111_111_111;
    let max: i64 = 99_999_999_999_999;
    let sets = generate_range(min, max, 16);

    let mut children = Vec::new();

    for (i, r) in sets.iter().copied().enumerate() {
        let this_inst = inst.to_owned();
        children.push(thread::spawn(move || {
            brute_force(i, up, this_inst, r.0, r.1)
        }));
    }

    let mut results = Vec::new();
    for child in children {
        if let Some(r) = child.join().unwrap() {
            results.push(r);
        }
    }

    println!("Brute force complete.");

    if up {
        println!("Smallest valid: {:?}", results.iter().min().unwrap());
    } else {
        println!("Largest valid: {:?}", results.iter().max().unwrap());
    }
}

fn brute_force(
    id: usize,
    up: bool,
    inst: Vec<Operation>,
    range_min: i64,
    range_max: i64,
) -> Option<i64> {
    let mut alu = ALU::new();

    if up {
        println!("Thread {:2} - Start {} to {}", id, range_min, range_max);
    } else {
        println!("Thread {:2} - Start {} to {}", id, range_max, range_min);
    }

    let range = if up {
        range_min..range_max
    } else {
        -range_max..range_min
    };

    'a: for i in range {
        let i_abs = i.abs();
        //println!("Thread {:2} - Test {}", id, i_abs);

        let mut test_input_int = i_abs;
        while test_input_int >= 10 {
            if test_input_int % 10 == 0 {
                continue 'a;
            }
            test_input_int /= 10;
        }

        alu.reset();
        alu.run(&inst, &mut int_to_que(i_abs));
        if alu.valid() {
            println!("Thread {:2} - First Valid: {}", id, i_abs);
            return Some(i_abs);
        }
    }

    println!("Thread {:2} - Finish", id);
    None
}

fn generate_range(min: i64, max: i64, num: i64) -> Vec<(i64, i64)> {
    let span: i64 = (max - min) / num;

    let mut sets = Vec::new();
    for i in 0..num {
        let n_min: i64 = min + i * span;
        let n_max: i64 = min + (i + 1) * span;
        sets.push((n_min, n_max));
    }

    if let Some(l) = sets.last_mut() {
        l.1 = max;
    }

    sets
}

pub fn int_to_que(input: i64) -> VecDeque<i32> {
    let mut out = VecDeque::new();

    let mut a = input;
    while a >= 1 {
        out.push_front((a % 10) as i32);
        a /= 10;
    }

    out
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Register {
    W,
    X,
    Y,
    Z,
}

impl Register {
    pub fn from_str(op_s: &str) -> Register {
        if op_s == "w" {
            Register::W
        } else if op_s == "x" {
            Register::X
        } else if op_s == "y" {
            Register::Y
        } else if op_s == "z" {
            Register::Z
        } else {
            panic!("Invalid register.");
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Operand {
    Reg(Register),
    Num(i32),
}

impl Operand {
    pub fn from_str(op_s: &str) -> Operand {
        if op_s == "w" || op_s == "x" || op_s == "y" || op_s == "z" {
            Operand::Reg(Register::from_str(op_s))
        } else {
            Operand::Num(op_s.parse().unwrap())
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Operation {
    Inp(Operand),
    Add((Operand, Operand)),
    Mul((Operand, Operand)),
    Div((Operand, Operand)),
    Mod((Operand, Operand)),
    Eql((Operand, Operand)),
}

impl Operation {
    pub fn from_str(op_s: &str) -> Operation {
        let segments: Vec<&str> = op_s.split(" ").collect();

        if segments[0] == "inp" {
            Operation::Inp(Operand::from_str(segments[1]))
        } else if segments[0] == "add" {
            Operation::Add((
                Operand::from_str(segments[1]),
                Operand::from_str(segments[2]),
            ))
        } else if segments[0] == "mul" {
            Operation::Mul((
                Operand::from_str(segments[1]),
                Operand::from_str(segments[2]),
            ))
        } else if segments[0] == "div" {
            Operation::Div((
                Operand::from_str(segments[1]),
                Operand::from_str(segments[2]),
            ))
        } else if segments[0] == "mod" {
            Operation::Mod((
                Operand::from_str(segments[1]),
                Operand::from_str(segments[2]),
            ))
        } else if segments[0] == "eql" {
            Operation::Eql((
                Operand::from_str(segments[1]),
                Operand::from_str(segments[2]),
            ))
        } else {
            panic!("Invalid operation.");
        }
    }
}

struct ALU {
    w: i64,
    x: i64,
    y: i64,
    z: i64,
}

impl ALU {
    pub fn new() -> ALU {
        ALU {
            w: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    }

    pub fn reset(&mut self) {
        self.w = 0;
        self.x = 0;
        self.y = 0;
        self.z = 0;
    }

    pub fn run(&mut self, inst: &Vec<Operation>, input: &mut VecDeque<i32>) {
        for i in inst.iter() {
            match i {
                Operation::Inp(a) => self.inp(a, input.pop_front().unwrap().into()),
                Operation::Add((a, b)) => self.add(a, b),
                Operation::Mul((a, b)) => self.mul(a, b),
                Operation::Div((a, b)) => self.div(a, b),
                Operation::Mod((a, b)) => self.modulo(a, b),
                Operation::Eql((a, b)) => self.eql(a, b),
            }
        }
    }

    fn inp(&mut self, a: &Operand, value: i64) {
        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w = value,
                Register::X => self.x = value,
                Register::Y => self.y = value,
                Register::Z => self.z = value,
            },
            _ => panic!("First operand must be a register."),
        }
    }

    fn get_operand(&self, a: &Operand) -> i64 {
        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w,
                Register::X => self.x,
                Register::Y => self.y,
                Register::Z => self.z,
            },
            Operand::Num(num) => (*num).into(),
        }
    }

    fn add(&mut self, a: &Operand, b: &Operand) {
        let second = self.get_operand(b);

        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w += second,
                Register::X => self.x += second,
                Register::Y => self.y += second,
                Register::Z => self.z += second,
            },
            _ => panic!("First operand must be a register."),
        }
    }

    fn mul(&mut self, a: &Operand, b: &Operand) {
        let second = self.get_operand(b);

        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w *= second,
                Register::X => self.x *= second,
                Register::Y => self.y *= second,
                Register::Z => self.z *= second,
            },
            _ => panic!("First operand must be a register."),
        }
    }

    fn div(&mut self, a: &Operand, b: &Operand) {
        let second = self.get_operand(b);

        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w /= second,
                Register::X => self.x /= second,
                Register::Y => self.y /= second,
                Register::Z => self.z /= second,
            },
            _ => panic!("First operand must be a register."),
        }
    }

    fn modulo(&mut self, a: &Operand, b: &Operand) {
        let second = self.get_operand(b);

        match a {
            Operand::Reg(reg) => match reg {
                Register::W => self.w %= second,
                Register::X => self.x %= second,
                Register::Y => self.y %= second,
                Register::Z => self.z %= second,
            },
            _ => panic!("First operand must be a register."),
        }
    }

    fn eql(&mut self, a: &Operand, b: &Operand) {
        let second = self.get_operand(b);

        match a {
            Operand::Reg(reg) => match reg {
                Register::W => {
                    if self.w == second {
                        self.w = 1
                    } else {
                        self.w = 0
                    }
                }
                Register::X => {
                    if self.x == second {
                        self.x = 1
                    } else {
                        self.x = 0
                    }
                }
                Register::Y => {
                    if self.y == second {
                        self.y = 1
                    } else {
                        self.y = 0
                    }
                }
                Register::Z => {
                    if self.z == second {
                        self.z = 1
                    } else {
                        self.z = 0
                    }
                }
            },
            _ => panic!("First operand must be a register."),
        }
    }

    pub fn valid(&self) -> bool {
        if self.z == 0 {
            true
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_int_to_que() {
        assert_eq!(int_to_que(123456), VecDeque::from([1, 2, 3, 4, 5, 6]));
    }

    #[test]
    fn test_register_from_str() {
        assert_eq!(Register::from_str(&"w"), Register::W);
        assert_eq!(Register::from_str(&"x"), Register::X);
        assert_eq!(Register::from_str(&"y"), Register::Y);
        assert_eq!(Register::from_str(&"z"), Register::Z);
    }

    #[test]
    fn test_operand_from_str() {
        assert_eq!(Operand::from_str(&"w"), Operand::Reg(Register::W));
        assert_eq!(Operand::from_str(&"x"), Operand::Reg(Register::X));
        assert_eq!(Operand::from_str(&"y"), Operand::Reg(Register::Y));
        assert_eq!(Operand::from_str(&"z"), Operand::Reg(Register::Z));
        assert_eq!(Operand::from_str(&"5"), Operand::Num(5));
        assert_eq!(Operand::from_str(&"-5"), Operand::Num(-5));
        assert_eq!(Operand::from_str(&"14"), Operand::Num(14));
    }

    #[test]
    fn test_operation_from_str() {
        assert_eq!(
            Operation::from_str(&"inp z"),
            Operation::Inp(Operand::Reg(Register::Z))
        );
        assert_eq!(
            Operation::from_str(&"add x y"),
            Operation::Add((Operand::Reg(Register::X), Operand::Reg(Register::Y)))
        );
        assert_eq!(
            Operation::from_str(&"mul z 5"),
            Operation::Mul((Operand::Reg(Register::Z), Operand::Num(5)))
        );
        assert_eq!(
            Operation::from_str(&"div x 2"),
            Operation::Div((Operand::Reg(Register::X), Operand::Num(2)))
        );
        assert_eq!(
            Operation::from_str(&"mod x w"),
            Operation::Mod((Operand::Reg(Register::X), Operand::Reg(Register::W)))
        );
        assert_eq!(
            Operation::from_str(&"eql x y"),
            Operation::Eql((Operand::Reg(Register::X), Operand::Reg(Register::Y)))
        );
    }

    #[test]
    fn test_alu_new() {
        let alu = ALU::new();
        assert_eq!(alu.w, 0);
        assert_eq!(alu.x, 0);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_inp() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "inp y".to_string(),
            "inp z".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(10);
        input.push_back(20);
        input.push_back(30);
        input.push_back(40);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 10);
        assert_eq!(alu.x, 20);
        assert_eq!(alu.y, 30);
        assert_eq!(alu.z, 40);
    }

    #[test]
    fn test_alu_add() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "add w x".to_string(),
            "add z 100".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(10);
        input.push_back(20);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 30);
        assert_eq!(alu.x, 20);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 100);
    }

    #[test]
    fn test_alu_mul() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "mul w x".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(10);
        input.push_back(20);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 200);
        assert_eq!(alu.x, 20);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_div() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "div w x".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(8);
        input.push_back(3);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 2);
        assert_eq!(alu.x, 3);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_mod() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "mod w x".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(7);
        input.push_back(3);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 1);
        assert_eq!(alu.x, 3);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_eql() {
        let lines = vec![
            "inp w".to_string(),
            "inp x".to_string(),
            "eql w x".to_string(),
            "eql y 0".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(7);
        input.push_back(3);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 0);
        assert_eq!(alu.x, 3);
        assert_eq!(alu.y, 1);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_example_1() {
        let lines = vec!["inp x".to_string(), "mul x -1".to_string()];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(7);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 0);
        assert_eq!(alu.x, -7);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 0);
    }

    #[test]
    fn test_alu_example_2() {
        let lines = vec![
            "inp z".to_string(),
            "inp x".to_string(),
            "mul z 3".to_string(),
            "eql z x".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(3);
        input.push_back(9);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 0);
        assert_eq!(alu.x, 9);
        assert_eq!(alu.y, 0);
        assert_eq!(alu.z, 1);
    }

    #[test]
    fn test_alu_example_3() {
        let lines = vec![
            "inp w".to_string(),
            "add z w".to_string(),
            "mod z 2".to_string(),
            "div w 2".to_string(),
            "add y w".to_string(),
            "mod y 2".to_string(),
            "div w 2".to_string(),
            "add x w".to_string(),
            "mod x 2".to_string(),
            "div w 2".to_string(),
            "mod w 2".to_string(),
        ];
        let inst = lines.iter().map(|s| Operation::from_str(s)).collect();
        let mut alu = ALU::new();
        let mut input = VecDeque::new();
        input.push_back(0b1010);
        alu.run(&inst, &mut input);
        assert_eq!(alu.w, 1);
        assert_eq!(alu.x, 0);
        assert_eq!(alu.y, 1);
        assert_eq!(alu.z, 0);
    }
}
