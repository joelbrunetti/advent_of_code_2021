//! --- Day 14: Extended Polymerization ---
//!
//! The incredible pressures at this depth are starting to put a strain on your submarine. The submarine has polymerization equipment that would produce suitable materials to reinforce the submarine, and the nearby volcanically-active caves should even have the necessary input elements in sufficient quantities.
//!
//! The submarine manual contains instructions for finding the optimal polymer formula; specifically, it offers a polymer template and a list of pair insertion rules (your puzzle input). You just need to work out what polymer would result after repeating the pair insertion process a few times.
//!
//! For example:
//!
//! NNCB
//!
//! CH -> B
//! HH -> N
//! CB -> H
//! NH -> C
//! HB -> C
//! HC -> B
//! HN -> C
//! NN -> C
//! BH -> H
//! NC -> B
//! NB -> B
//! BN -> B
//! BB -> N
//! BC -> B
//! CC -> N
//! CN -> C
//!
//! The first line is the polymer template - this is the starting point of the process.
//!
//! The following section defines the pair insertion rules. A rule like AB -> C means that when elements A and B are immediately adjacent, element C should be inserted between them. These insertions all happen simultaneously.
//!
//! So, starting with the polymer template NNCB, the first step simultaneously considers all three pairs:
//!
//!     The first pair (NN) matches the rule NN -> C, so element C is inserted between the first N and the second N.
//!     The second pair (NC) matches the rule NC -> B, so element B is inserted between the N and the C.
//!     The third pair (CB) matches the rule CB -> H, so element H is inserted between the C and the B.
//!
//! Note that these pairs overlap: the second element of one pair is the first element of the next pair. Also, because all pairs are considered simultaneously, inserted elements are not considered to be part of a pair until the next step.
//!
//! After the first step of this process, the polymer becomes NCNBCHB.
//!
//! Here are the results of a few steps using the above rules:
//!
//! Template:     NNCB
//! After step 1: NCNBCHB
//! After step 2: NBCCNBBBCBHCB
//! After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
//! After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
//!
//! This polymer grows quickly. After step 5, it has length 97; After step 10, it has length 3073. After step 10, B occurs 1749 times, C occurs 298 times, H occurs 161 times, and N occurs 865 times; taking the quantity of the most common element (B, 1749) and subtracting the quantity of the least common element (H, 161) produces 1749 - 161 = 1588.
//!
//! Apply 10 steps of pair insertion to the polymer template and find the most and least common elements in the result. What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
//!
//!--- Part Two ---
//!
//! The resulting polymer isn't nearly strong enough to reinforce the submarine. You'll need to run more steps of the pair insertion process; a total of 40 steps should do it.
//!
//! In the above example, the most common element is B (occurring 2192039569602 times) and the least common element is H (occurring 3849876073 times); subtracting these produces 2188189693529.
//!
//! Apply 40 steps of pair insertion to the polymer template and find the most and least common elements in the result. What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
//!

use std::collections::HashMap;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day13.input").unwrap();
    let mut parts = lines.split(|l| l == "");
    let poly_str = parts.next().unwrap().iter().next().unwrap().as_str();
    let rules_vec = parts.next().unwrap();

    let mut p = Poly::from_str(poly_str);
    p.add_rules(rules_vec);

    p.steps(10);
    let (min, max) = p.find_min_max();
    println!("10 Steps: Most common - least common: {}", max - min);

    p.steps(30);
    let (min, max) = p.find_min_max();
    println!("40 Steps: Most common - least common: {}", max - min);
}

#[derive(Debug)]
struct Poly {
    count: HashMap<(char, char), u64>,
    rules: HashMap<(char, char), char>,
}

impl Poly {
    pub fn from_str(init: &str) -> Poly {
        let mut count = HashMap::new();
        let rules = HashMap::new();

        for pair in init.chars().zip(init.chars().skip(1)) {
            let pair_count = count.entry(pair).or_insert(0);
            *pair_count += 1;
        }
        count.insert((init.chars().last().unwrap(), '_'), 1);

        Poly {
            count: count,
            rules: rules,
        }
    }

    pub fn add_rules(&mut self, init: &[String]) {
        for r in init.iter() {
            let mut s = r.split(" -> ");
            let mut l = s.next().unwrap().chars();
            let l1 = l.next().unwrap();
            let l2 = l.next().unwrap();
            let r = s.next().unwrap().chars().next().unwrap();
            self.rules.insert((l1, l2), r);
        }
    }

    fn step(&mut self) {
        let full_count = self.count.clone();
        for (pair, count) in full_count.iter() {
            if let Some(insert) = self.rules.get(pair) {
                let new_one = (pair.0, *insert);
                let new_two = (*insert, pair.1);
                let tmp = self.count.entry(new_one).or_insert(0);
                *tmp += count;
                let tmp = self.count.entry(new_two).or_insert(0);
                *tmp += count;
                let tmp = self.count.get_mut(pair).unwrap();
                *tmp -= count;
            }
        }
    }

    pub fn steps(&mut self, steps: u32) {
        for _i in 0..steps {
            self.step();
        }
    }

    pub fn find_min_max(&self) -> (u64, u64) {
        let mut firsts = HashMap::new();

        for (pair, count) in self.count.iter() {
            let first_count = firsts.entry(pair.0).or_insert(0);
            *first_count += count;
        }

        let max = firsts.values().max().unwrap();
        let min = firsts.values().min().unwrap();
        (*min, *max)
    }

    #[allow(dead_code)]
    pub fn len(&self) -> u64 {
        self.count.values().sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_poly_new() {
        let p = Poly::from_str(&"NNCB");
        assert_eq!(p.len(), 4);
    }

    #[test]
    fn test_poly_find_min_max() {
        let p = Poly::from_str(&"NNCB");
        assert_eq!(p.find_min_max(), (1, 2));
    }

    #[test]
    fn test_poly_add_rules() {
        let mut p = Poly::from_str(&"NNCB");
        let r = vec![
            "CH -> B".to_string(),
            "HH -> N".to_string(),
            "CB -> H".to_string(),
            "NH -> C".to_string(),
            "HB -> C".to_string(),
            "HC -> B".to_string(),
            "HN -> C".to_string(),
            "NN -> C".to_string(),
            "BH -> H".to_string(),
            "NC -> B".to_string(),
            "NB -> B".to_string(),
            "BN -> B".to_string(),
            "BB -> N".to_string(),
            "BC -> B".to_string(),
            "CC -> N".to_string(),
            "CN -> C".to_string(),
        ];
        p.add_rules(&r[..]);
        assert_eq!(p.rules.len(), 16);
    }

    #[test]
    fn test_poly_step() {
        let mut p = Poly::from_str(&"NNCB");
        let r = vec![
            "CH -> B".to_string(),
            "HH -> N".to_string(),
            "CB -> H".to_string(),
            "NH -> C".to_string(),
            "HB -> C".to_string(),
            "HC -> B".to_string(),
            "HN -> C".to_string(),
            "NN -> C".to_string(),
            "BH -> H".to_string(),
            "NC -> B".to_string(),
            "NB -> B".to_string(),
            "BN -> B".to_string(),
            "BB -> N".to_string(),
            "BC -> B".to_string(),
            "CC -> N".to_string(),
            "CN -> C".to_string(),
        ];
        p.add_rules(&r[..]);
        assert_eq!(p.len(), 4);
        p.steps(5);
        assert_eq!(p.len(), 97);
        p.steps(5);
        assert_eq!(p.find_min_max(), (161, 1749));
        p.steps(30);
        assert_eq!(p.find_min_max(), (3849876073, 2192039569602));
    }
}
