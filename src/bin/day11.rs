//! --- Day 12: Passage Pathing ---
//!
//! With your submarine's subterranean subsystems subsisting suboptimally, the only way you're getting out of this cave anytime soon is by finding a path yourself. Not just a path - the only way to know if you've found the best path is to find all of them.
//!
//! Fortunately, the sensors are still mostly working, and so you build a rough map of the remaining caves (your puzzle input). For example:
//!
//! start-A
//! start-b
//! A-c
//! A-b
//! b-d
//! A-end
//! b-end
//!
//! This is a list of how all of the caves are connected. You start in the cave named start, and your destination is the cave named end. An entry like b-d means that cave b is connected to cave d - that is, you can move between them.
//!
//! So, the above cave system looks roughly like this:
//!
//!     start
//!     /   \
//! c--A-----b--d
//!     \   /
//!      end
//!
//! Your goal is to find the number of distinct paths that start at start, end at end, and don't visit small caves more than once. There are two types of caves: big caves (written in uppercase, like A) and small caves (written in lowercase, like b). It would be a waste of time to visit any small cave more than once, but big caves are large enough that it might be worth visiting them multiple times. So, all paths you find should visit small caves at most once, and can visit big caves any number of times.
//!
//! Given these rules, there are 10 paths through this example cave system:
//!
//! start,A,b,A,c,A,end
//! start,A,b,A,end
//! start,A,b,end
//! start,A,c,A,b,A,end
//! start,A,c,A,b,end
//! start,A,c,A,end
//! start,A,end
//! start,b,A,c,A,end
//! start,b,A,end
//! start,b,end
//!
//! (Each line in the above list corresponds to a single path; the caves visited by that path are listed in the order they are visited and separated by commas.)
//!
//! Note that in this cave system, cave d is never visited by any path: to do so, cave b would need to be visited twice (once on the way to cave d and a second time when returning from cave d), and since cave b is small, this is not allowed.
//!
//! Here is a slightly larger example:
//!
//! dc-end
//! HN-start
//! start-kj
//! dc-start
//! dc-HN
//! LN-dc
//! HN-end
//! kj-sa
//! kj-HN
//! kj-dc
//!
//! The 19 paths through it are as follows:
//!
//! start,HN,dc,HN,end
//! start,HN,dc,HN,kj,HN,end
//! start,HN,dc,end
//! start,HN,dc,kj,HN,end
//! start,HN,end
//! start,HN,kj,HN,dc,HN,end
//! start,HN,kj,HN,dc,end
//! start,HN,kj,HN,end
//! start,HN,kj,dc,HN,end
//! start,HN,kj,dc,end
//! start,dc,HN,end
//! start,dc,HN,kj,HN,end
//! start,dc,end
//! start,dc,kj,HN,end
//! start,kj,HN,dc,HN,end
//! start,kj,HN,dc,end
//! start,kj,HN,end
//! start,kj,dc,HN,end
//! start,kj,dc,end
//!
//! Finally, this even larger example has 226 paths through it:
//!
//! fs-end
//! he-DX
//! fs-he
//! start-DX
//! pj-DX
//! end-zg
//! zg-sl
//! zg-pj
//! pj-he
//! RW-he
//! fs-DX
//! pj-RW
//! zg-RW
//! start-pj
//! he-WI
//! zg-he
//! pj-fs
//! start-RW
//!
//! How many paths through this cave system are there that visit small caves at most once?
//!
//!--- Part Two ---
//!
//! After reviewing the available paths, you realize you might have time to visit a single small cave twice. Specifically, big caves can be visited any number of times, a single small cave can be visited at most twice, and the remaining small caves can be visited at most once. However, the caves named start and end can only be visited exactly once each: once you leave the start cave, you may not return to it, and once you reach the end cave, the path must end immediately.
//!
//! Now, the 36 possible paths through the first example above are:
//!
//! start,A,b,A,b,A,c,A,end
//! start,A,b,A,b,A,end
//! start,A,b,A,b,end
//! start,A,b,A,c,A,b,A,end
//! start,A,b,A,c,A,b,end
//! start,A,b,A,c,A,c,A,end
//! start,A,b,A,c,A,end
//! start,A,b,A,end
//! start,A,b,d,b,A,c,A,end
//! start,A,b,d,b,A,end
//! start,A,b,d,b,end
//! start,A,b,end
//! start,A,c,A,b,A,b,A,end
//! start,A,c,A,b,A,b,end
//! start,A,c,A,b,A,c,A,end
//! start,A,c,A,b,A,end
//! start,A,c,A,b,d,b,A,end
//! start,A,c,A,b,d,b,end
//! start,A,c,A,b,end
//! start,A,c,A,c,A,b,A,end
//! start,A,c,A,c,A,b,end
//! start,A,c,A,c,A,end
//! start,A,c,A,end
//! start,A,end
//! start,b,A,b,A,c,A,end
//! start,b,A,b,A,end
//! start,b,A,b,end
//! start,b,A,c,A,b,A,end
//! start,b,A,c,A,b,end
//! start,b,A,c,A,c,A,end
//! start,b,A,c,A,end
//! start,b,A,end
//! start,b,d,b,A,c,A,end
//! start,b,d,b,A,end
//! start,b,d,b,end
//! start,b,end
//!
//! The slightly larger example above now has 103 paths through it, and the even larger example now has 3509 paths through it.
//!
//! Given these new rules, how many paths through this cave system are there?
//!

use std::collections::{HashMap, HashSet, VecDeque};

fn main() {
    let lines = advent_of_code_2021::input_file("input/day11.input").unwrap();
    let mut g = Graph::from_lines(lines);
    println!("Number of Paths {}", g.search(&"start", &"end", false));
    println!(
        "Number of Paths with one double {}",
        g.search(&"start", &"end", true)
    );
}

#[derive(Debug)]
struct Node {
    id: String,
    is_small: bool,
    children: Vec<String>,
}

impl Node {
    pub fn new(id: &str) -> Node {
        Node {
            id: id.to_string(),
            is_small: id.chars().next().unwrap().is_ascii_lowercase(),
            children: vec![],
        }
    }
}

#[derive(Debug)]
struct Graph {
    nodes: HashMap<String, Node>,
}

impl Graph {
    #[allow(dead_code)]
    pub fn new() -> Graph {
        Graph {
            nodes: HashMap::new(),
        }
    }

    pub fn from_lines(lines: Vec<String>) -> Graph {
        let mut g = Graph {
            nodes: HashMap::new(),
        };

        for l in lines.iter() {
            let mut s = l.split("-");
            let l = s.next().unwrap();
            let r = s.next().unwrap();
            g.add_edge(l, r);
        }

        g
    }

    pub fn add_edge(&mut self, a: &str, b: &str) {
        if let Some(node_a) = self.nodes.get_mut(a) {
            node_a.children.push(b.to_string());
        } else {
            let mut n = Node::new(a);
            n.children.push(b.to_string());
            self.nodes.insert(a.to_string(), n);
        }

        if let Some(node_b) = self.nodes.get_mut(b) {
            node_b.children.push(a.to_string());
        } else {
            let mut n = Node::new(b);
            n.children.push(a.to_string());
            self.nodes.insert(b.to_string(), n);
        }
    }

    fn has_double(path: &Vec<String>) -> bool {
        let mut set = HashSet::new();
        for node in path.iter() {
            if node.chars().next().unwrap().is_ascii_lowercase() {
                if !set.insert(node) {
                    return true;
                }
            }
        }
        false
    }

    pub fn search(&mut self, start: &str, end: &str, allow_double: bool) -> i32 {
        let mut paths_sum = 0;
        let mut queue: VecDeque<Vec<String>> = VecDeque::new();
        queue.push_back(vec![start.to_string()]);

        while let Some(path) = queue.pop_front() {
            if let Some(head) = path.last() {
                if let Some(node) = self.nodes.get(head) {
                    for c in node.children.iter() {
                        if c == start {
                            continue;
                        } else if c == end {
                            paths_sum += 1;
                            continue;
                        } else if self.nodes.get(c).unwrap().is_small {
                            let visits: i32 = (path.iter().filter(|x| *x == c).count() as i32)
                                .try_into()
                                .unwrap();
                            if allow_double {
                                if Graph::has_double(&path) {
                                    if visits >= 1 {
                                        continue;
                                    }
                                } else {
                                    if visits >= 2 {
                                        continue;
                                    }
                                }
                            } else {
                                if visits >= 1 {
                                    continue;
                                }
                            }
                        }

                        let mut add_path = path.clone();
                        add_path.push(c.clone());
                        queue.push_back(add_path);
                    }
                }
            }
        }

        paths_sum
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_node_new() {
        let n = Node::new(&"test");
        assert_eq!(n.id, "test");
    }

    #[test]
    fn test_graph_new() {
        let g = Graph::new();
        assert_eq!(g.nodes.len(), 0);
    }

    #[test]
    fn test_graph_edge() {
        let mut g = Graph::new();
        g.add_edge(&"start", &"A");
        g.add_edge(&"end", &"A");
        assert_eq!(g.nodes.len(), 3);
        assert_eq!(g.nodes.get("start").unwrap().children, vec!["A"]);
        assert_eq!(g.nodes.get("A").unwrap().children, vec!["start", "end"]);
        assert_eq!(g.nodes.get("end").unwrap().children, vec!["A"]);
    }

    #[test]
    fn test_graph_edge_sample() {
        let mut g = Graph::new();
        g.add_edge(&"start", &"A");
        g.add_edge(&"start", &"b");
        g.add_edge(&"A", &"c");
        g.add_edge(&"A", &"b");
        g.add_edge(&"b", &"d");
        g.add_edge(&"A", &"end");
        g.add_edge(&"b", &"end");
        assert_eq!(g.nodes.len(), 6);
        assert_eq!(g.nodes.get("start").unwrap().children, vec!["A", "b"]);
        assert_eq!(
            g.nodes.get("A").unwrap().children,
            vec!["start", "c", "b", "end"]
        );
        assert_eq!(g.nodes.get("end").unwrap().children, vec!["A", "b"]);
    }

    #[test]
    fn test_search_sample_one() {
        let mut g = Graph::new();
        g.add_edge(&"start", &"A");
        g.add_edge(&"start", &"b");
        g.add_edge(&"A", &"c");
        g.add_edge(&"A", &"b");
        g.add_edge(&"b", &"d");
        g.add_edge(&"A", &"end");
        g.add_edge(&"b", &"end");
        let paths_sum = g.search(&"start", &"end", false);
        assert_eq!(paths_sum, 10);
    }

    #[test]
    fn test_search_sample_two() {
        let mut g = Graph::new();
        g.add_edge(&"start", &"A");
        g.add_edge(&"start", &"b");
        g.add_edge(&"A", &"c");
        g.add_edge(&"A", &"b");
        g.add_edge(&"b", &"d");
        g.add_edge(&"A", &"end");
        g.add_edge(&"b", &"end");
        let paths_sum = g.search(&"start", &"end", true);
        assert_eq!(paths_sum, 36);
    }
}
