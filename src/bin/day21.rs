//! --- Day 22: Reactor Reboot ---
//!
//! Operating at these extreme ocean depths has overloaded the submarine's reactor; it needs to be rebooted.
//!
//! The reactor core is made up of a large 3-dimensional grid made up entirely of cubes, one cube per integer 3-dimensional coordinate (x,y,z). Each cube can be either on or off; at the start of the reboot process, they are all off. (Could it be an old model of a reactor you've seen before?)
//!
//! To reboot the reactor, you just need to set all of the cubes to either on or off by following a list of reboot steps (your puzzle input). Each step specifies a cuboid (the set of all cubes that have coordinates which fall within ranges for x, y, and z) and whether to turn all of the cubes in that cuboid on or off.
//!
//! For example, given these reboot steps:
//!
//! on x=10..12,y=10..12,z=10..12
//! on x=11..13,y=11..13,z=11..13
//! off x=9..11,y=9..11,z=9..11
//! on x=10..10,y=10..10,z=10..10
//!
//! The first step (on x=10..12,y=10..12,z=10..12) turns on a 3x3x3 cuboid consisting of 27 cubes:
//!
//!     10,10,10
//!     10,10,11
//!     10,10,12
//!     10,11,10
//!     10,11,11
//!     10,11,12
//!     10,12,10
//!     10,12,11
//!     10,12,12
//!     11,10,10
//!     11,10,11
//!     11,10,12
//!     11,11,10
//!     11,11,11
//!     11,11,12
//!     11,12,10
//!     11,12,11
//!     11,12,12
//!     12,10,10
//!     12,10,11
//!     12,10,12
//!     12,11,10
//!     12,11,11
//!     12,11,12
//!     12,12,10
//!     12,12,11
//!     12,12,12
//!
//! The second step (on x=11..13,y=11..13,z=11..13) turns on a 3x3x3 cuboid that overlaps with the first. As a result, only 19 additional cubes turn on; the rest are already on from the previous step:
//!
//!     11,11,13
//!     11,12,13
//!     11,13,11
//!     11,13,12
//!     11,13,13
//!     12,11,13
//!     12,12,13
//!     12,13,11
//!     12,13,12
//!     12,13,13
//!     13,11,11
//!     13,11,12
//!     13,11,13
//!     13,12,11
//!     13,12,12
//!     13,12,13
//!     13,13,11
//!     13,13,12
//!     13,13,13
//!
//! The third step (off x=9..11,y=9..11,z=9..11) turns off a 3x3x3 cuboid that overlaps partially with some cubes that are on, ultimately turning off 8 cubes:
//!
//!     10,10,10
//!     10,10,11
//!     10,11,10
//!     10,11,11
//!     11,10,10
//!     11,10,11
//!     11,11,10
//!     11,11,11
//!
//! The final step (on x=10..10,y=10..10,z=10..10) turns on a single cube, 10,10,10. After this last step, 39 cubes are on.
//!
//! The initialization procedure only uses cubes that have x, y, and z positions of at least -50 and at most 50. For now, ignore cubes outside this region.
//!
//! Here is a larger example:
//!
//! on x=-20..26,y=-36..17,z=-47..7
//! on x=-20..33,y=-21..23,z=-26..28
//! on x=-22..28,y=-29..23,z=-38..16
//! on x=-46..7,y=-6..46,z=-50..-1
//! on x=-49..1,y=-3..46,z=-24..28
//! on x=2..47,y=-22..22,z=-23..27
//! on x=-27..23,y=-28..26,z=-21..29
//! on x=-39..5,y=-6..47,z=-3..44
//! on x=-30..21,y=-8..43,z=-13..34
//! on x=-22..26,y=-27..20,z=-29..19
//! off x=-48..-32,y=26..41,z=-47..-37
//! on x=-12..35,y=6..50,z=-50..-2
//! off x=-48..-32,y=-32..-16,z=-15..-5
//! on x=-18..26,y=-33..15,z=-7..46
//! off x=-40..-22,y=-38..-28,z=23..41
//! on x=-16..35,y=-41..10,z=-47..6
//! off x=-32..-23,y=11..30,z=-14..3
//! on x=-49..-5,y=-3..45,z=-29..18
//! off x=18..30,y=-20..-8,z=-3..13
//! on x=-41..9,y=-7..43,z=-33..15
//! on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
//! on x=967..23432,y=45373..81175,z=27513..53682
//!
//! The last two steps are fully outside the initialization procedure area; all other steps are fully within it. After executing these steps in the initialization procedure region, 590784 cubes are on.
//!
//! Execute the reboot steps. Afterward, considering only cubes in the region x=-50..50,y=-50..50,z=-50..50, how many cubes are on?
//!
//!--- Part Two ---
//!
//! Now that the initialization procedure is complete, you can reboot the reactor.
//!
//! Starting with all cubes off, run all of the reboot steps for all cubes in the reactor.
//!
//! Consider the following reboot steps:
//!
//! on x=-5..47,y=-31..22,z=-19..33
//! on x=-44..5,y=-27..21,z=-14..35
//! on x=-49..-1,y=-11..42,z=-10..38
//! on x=-20..34,y=-40..6,z=-44..1
//! off x=26..39,y=40..50,z=-2..11
//! on x=-41..5,y=-41..6,z=-36..8
//! off x=-43..-33,y=-45..-28,z=7..25
//! on x=-33..15,y=-32..19,z=-34..11
//! off x=35..47,y=-46..-34,z=-11..5
//! on x=-14..36,y=-6..44,z=-16..29
//! on x=-57795..-6158,y=29564..72030,z=20435..90618
//! on x=36731..105352,y=-21140..28532,z=16094..90401
//! on x=30999..107136,y=-53464..15513,z=8553..71215
//! on x=13528..83982,y=-99403..-27377,z=-24141..23996
//! on x=-72682..-12347,y=18159..111354,z=7391..80950
//! on x=-1060..80757,y=-65301..-20884,z=-103788..-16709
//! on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856
//! on x=-52752..22273,y=-49450..9096,z=54442..119054
//! on x=-29982..40483,y=-108474..-28371,z=-24328..38471
//! on x=-4958..62750,y=40422..118853,z=-7672..65583
//! on x=55694..108686,y=-43367..46958,z=-26781..48729
//! on x=-98497..-18186,y=-63569..3412,z=1232..88485
//! on x=-726..56291,y=-62629..13224,z=18033..85226
//! on x=-110886..-34664,y=-81338..-8658,z=8914..63723
//! on x=-55829..24974,y=-16897..54165,z=-121762..-28058
//! on x=-65152..-11147,y=22489..91432,z=-58782..1780
//! on x=-120100..-32970,y=-46592..27473,z=-11695..61039
//! on x=-18631..37533,y=-124565..-50804,z=-35667..28308
//! on x=-57817..18248,y=49321..117703,z=5745..55881
//! on x=14781..98692,y=-1341..70827,z=15753..70151
//! on x=-34419..55919,y=-19626..40991,z=39015..114138
//! on x=-60785..11593,y=-56135..2999,z=-95368..-26915
//! on x=-32178..58085,y=17647..101866,z=-91405..-8878
//! on x=-53655..12091,y=50097..105568,z=-75335..-4862
//! on x=-111166..-40997,y=-71714..2688,z=5609..50954
//! on x=-16602..70118,y=-98693..-44401,z=5197..76897
//! on x=16383..101554,y=4615..83635,z=-44907..18747
//! off x=-95822..-15171,y=-19987..48940,z=10804..104439
//! on x=-89813..-14614,y=16069..88491,z=-3297..45228
//! on x=41075..99376,y=-20427..49978,z=-52012..13762
//! on x=-21330..50085,y=-17944..62733,z=-112280..-30197
//! on x=-16478..35915,y=36008..118594,z=-7885..47086
//! off x=-98156..-27851,y=-49952..43171,z=-99005..-8456
//! off x=2032..69770,y=-71013..4824,z=7471..94418
//! on x=43670..120875,y=-42068..12382,z=-24787..38892
//! off x=37514..111226,y=-45862..25743,z=-16714..54663
//! off x=25699..97951,y=-30668..59918,z=-15349..69697
//! off x=-44271..17935,y=-9516..60759,z=49131..112598
//! on x=-61695..-5813,y=40978..94975,z=8655..80240
//! off x=-101086..-9439,y=-7088..67543,z=33935..83858
//! off x=18020..114017,y=-48931..32606,z=21474..89843
//! off x=-77139..10506,y=-89994..-18797,z=-80..59318
//! off x=8476..79288,y=-75520..11602,z=-96624..-24783
//! on x=-47488..-1262,y=24338..100707,z=16292..72967
//! off x=-84341..13987,y=2429..92914,z=-90671..-1318
//! off x=-37810..49457,y=-71013..-7894,z=-105357..-13188
//! off x=-27365..46395,y=31009..98017,z=15428..76570
//! off x=-70369..-16548,y=22648..78696,z=-1892..86821
//! on x=-53470..21291,y=-120233..-33476,z=-44150..38147
//! off x=-93533..-4276,y=-16170..68771,z=-104985..-24507
//!
//! After running the above reboot steps, 2758514936282235 cubes are on. (Just for fun, 474140 of those are also in the initialization procedure region.)
//!
//! Starting again with all cubes off, execute all reboot steps. Afterward, considering all cubes, how many cubes are on?
//!

use std::collections::HashMap;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day21.input").unwrap();
    let mut r = Reactor::from_lines(&lines);
    let on = r.start(true);
    println!("Init Cubes on: {}", on);
    let on = r.start(false);
    println!("Final Cubes on: {}", on);
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Cube {
    on: bool,
    x: (i64, i64),
    y: (i64, i64),
    z: (i64, i64),
}

impl Cube {
    pub fn from_string(line: &String) -> Cube {
        let mut cube = Cube {
            on: false,
            x: (0, 0),
            y: (0, 0),
            z: (0, 0),
        };

        let mut a = line.split(" ");

        let s = a.next().unwrap();
        if s == "on" {
            cube.on = true;
        } else if s == "off" {
            cube.on = false;
        } else {
            panic!("Invalid on string.");
        }

        let mut b = a.next().unwrap().split(",");

        let mut lim_iter = b.next().unwrap().strip_prefix("x=").unwrap().split("..");
        cube.x.0 = lim_iter.next().unwrap().parse().unwrap();
        cube.x.1 = lim_iter.next().unwrap().parse().unwrap();

        let mut lim_iter = b.next().unwrap().strip_prefix("y=").unwrap().split("..");
        cube.y.0 = lim_iter.next().unwrap().parse().unwrap();
        cube.y.1 = lim_iter.next().unwrap().parse().unwrap();

        let mut lim_iter = b.next().unwrap().strip_prefix("z=").unwrap().split("..");
        cube.z.0 = lim_iter.next().unwrap().parse().unwrap();
        cube.z.1 = lim_iter.next().unwrap().parse().unwrap();

        cube
    }

    pub fn within(&self, bound: i64) -> bool {
        if self.x.0 < -bound
            || self.x.1 > bound
            || self.y.0 < -bound
            || self.y.1 > bound
            || self.z.0 < -bound
            || self.z.1 > bound
        {
            false
        } else {
            true
        }
    }

    pub fn intersection(&self, existing: &Cube) -> Cube {
        Cube {
            on: false,
            x: (
                std::cmp::max(self.x.0, existing.x.0),
                std::cmp::min(self.x.1, existing.x.1),
            ),
            y: (
                std::cmp::max(self.y.0, existing.y.0),
                std::cmp::min(self.y.1, existing.y.1),
            ),
            z: (
                std::cmp::max(self.z.0, existing.z.0),
                std::cmp::min(self.z.1, existing.z.1),
            ),
        }
    }

    pub fn is_empty(&self) -> bool {
        if self.x.0 <= self.x.1 && self.y.0 <= self.y.1 && self.z.0 <= self.z.1 {
            false
        } else {
            true
        }
    }

    pub fn volume(&self) -> i64 {
        if self.is_empty() {
            0
        } else {
            (1 + self.x.1 - self.x.0) * (1 + self.y.1 - self.y.0) * (1 + self.z.1 - self.z.0)
        }
    }
}

struct Reactor {
    instr: Vec<Cube>,
    volumes: HashMap<Cube, i64>,
}

impl Reactor {
    pub fn from_lines(lines: &Vec<String>) -> Reactor {
        let r = Reactor {
            instr: lines.iter().map(|c| Cube::from_string(c)).collect(),
            volumes: HashMap::new(),
        };

        r
    }

    pub fn start(&mut self, init: bool) -> i64 {
        self.volumes.clear();

        for step in self.instr.iter() {
            if init && !step.within(50) {
                continue;
            }

            let mut more_volumes = HashMap::new();

            for (existing, total) in self.volumes.iter() {
                let neg = step.intersection(&existing);

                if !neg.is_empty() {
                    let count = more_volumes.entry(neg).or_insert(0);
                    *count -= total;
                }
            }

            if step.on {
                let count = more_volumes.entry(step.clone()).or_insert(0);
                *count += 1;
            }

            let added = more_volumes
                .iter()
                .map(|(v, c)| v.volume() * c)
                .sum::<i64>();

            for (this_cube, this_count) in more_volumes.iter() {
                let count = self.volumes.entry(this_cube.clone()).or_insert(0);
                *count += this_count;
            }

            println!(
                "New Volume: {} after adding {}",
                self.volumes
                    .iter()
                    .map(|(v, c)| v.volume() * c)
                    .sum::<i64>(),
                added
            );
        }

        self.volumes.iter().map(|(v, c)| v.volume() * c).sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cube_from_string_on() {
        let s = Cube::from_string(&"on x=10..12,y=10..12,z=10..12".to_string());
        assert_eq!(s.on, true);
        assert_eq!(s.x, (10, 12));
        assert_eq!(s.y, (10, 12));
        assert_eq!(s.z, (10, 12));
    }

    #[test]
    fn test_cube_from_string_off() {
        let s = Cube::from_string(&"off x=9..11,y=9..11,z=9..11".to_string());
        assert_eq!(s.on, false);
        assert_eq!(s.x, (9, 11));
        assert_eq!(s.y, (9, 11));
        assert_eq!(s.z, (9, 11));
    }

    #[test]
    fn test_cube_within() {
        let a = Cube::from_string(&"off x=-5..5,y=-5..5,z=-5..5".to_string());
        assert!(a.within(50));
        let b = Cube::from_string(&"off x=-50..5,y=-5..50,z=-5..5".to_string());
        assert!(b.within(50));
        let c = Cube::from_string(&"off x=-50..5,y=-5..50,z=-5..51".to_string());
        assert!(!c.within(50));
    }

    #[test]
    fn test_cube_intersection() {
        let big = Cube::from_string(&"off x=-5..5,y=-5..5,z=-5..5".to_string());
        let right = Cube::from_string(&"off x=0..3,y=0..5,z=4..8".to_string());
        let inter = Cube::from_string(&"off x=0..3,y=0..5,z=4..5".to_string());
        assert_eq!(big.intersection(&right), inter);
    }

    #[test]
    fn test_cube_is_empty() {
        let big = Cube::from_string(&"on x=-5..5,y=-5..5,z=-5..5".to_string());
        assert!(!big.is_empty());
        let empty = Cube::from_string(&"on x=-5..-6,y=-5..5,z=-5..5".to_string());
        assert!(empty.is_empty());
    }

    #[test]
    fn test_cube_volume() {
        let big = Cube::from_string(&"on x=-5..5,y=-5..5,z=-5..5".to_string());
        assert_eq!(big.volume(), 1331);

        let empty = Cube::from_string(&"on x=-5..-6,y=-5..5,z=-5..5".to_string());
        assert_eq!(empty.volume(), 0);

        let partial = Cube::from_string(&"on x=5..5,y=-5..5,z=0..5".to_string());
        assert_eq!(partial.volume(), 1 * 11 * 6);
    }

    #[test]
    fn test_reactor_from_lines() {
        let l = vec![
            "on x=10..12,y=10..12,z=10..12".to_string(),
            "on x=11..13,y=11..13,z=11..13".to_string(),
            "off x=9..11,y=9..11,z=9..11".to_string(),
            "on x=10..10,y=10..10,z=10..10".to_string(),
        ];

        let r = Reactor::from_lines(&l);
        assert_eq!(r.instr.len(), 4);
    }

    #[test]
    fn test_reactor_start() {
        let l = vec![
            "on x=10..12,y=10..12,z=10..12".to_string(),
            "on x=11..13,y=11..13,z=11..13".to_string(),
            "off x=9..11,y=9..11,z=9..11".to_string(),
            "on x=10..10,y=10..10,z=10..10".to_string(),
        ];

        let mut r = Reactor::from_lines(&l);
        assert_eq!(r.start(true), 39);
    }

    #[test]
    fn test_reactor_start_large() {
        let l = vec![
            "on x=-20..26,y=-36..17,z=-47..7".to_string(),
            "on x=-20..33,y=-21..23,z=-26..28".to_string(),
            "on x=-22..28,y=-29..23,z=-38..16".to_string(),
            "on x=-46..7,y=-6..46,z=-50..-1".to_string(),
            "on x=-49..1,y=-3..46,z=-24..28".to_string(),
            "on x=2..47,y=-22..22,z=-23..27".to_string(),
            "on x=-27..23,y=-28..26,z=-21..29".to_string(),
            "on x=-39..5,y=-6..47,z=-3..44".to_string(),
            "on x=-30..21,y=-8..43,z=-13..34".to_string(),
            "on x=-22..26,y=-27..20,z=-29..19".to_string(),
            "off x=-48..-32,y=26..41,z=-47..-37".to_string(),
            "on x=-12..35,y=6..50,z=-50..-2".to_string(),
            "off x=-48..-32,y=-32..-16,z=-15..-5".to_string(),
            "on x=-18..26,y=-33..15,z=-7..46".to_string(),
            "off x=-40..-22,y=-38..-28,z=23..41".to_string(),
            "on x=-16..35,y=-41..10,z=-47..6".to_string(),
            "off x=-32..-23,y=11..30,z=-14..3".to_string(),
            "on x=-49..-5,y=-3..45,z=-29..18".to_string(),
            "off x=18..30,y=-20..-8,z=-3..13".to_string(),
            "on x=-41..9,y=-7..43,z=-33..15".to_string(),
            "on x=-54112..-39298,y=-85059..-49293,z=-27449..7877".to_string(),
            "on x=967..23432,y=45373..81175,z=27513..53682".to_string(),
        ];

        let mut r = Reactor::from_lines(&l);
        assert_eq!(r.start(true), 590784);
    }

    #[test]
    fn test_reactor_start_big() {
        let l = vec![
            "on x=-5..47,y=-31..22,z=-19..33".to_string(),
            "on x=-44..5,y=-27..21,z=-14..35".to_string(),
            "on x=-49..-1,y=-11..42,z=-10..38".to_string(),
            "on x=-20..34,y=-40..6,z=-44..1".to_string(),
            "off x=26..39,y=40..50,z=-2..11".to_string(),
            "on x=-41..5,y=-41..6,z=-36..8".to_string(),
            "off x=-43..-33,y=-45..-28,z=7..25".to_string(),
            "on x=-33..15,y=-32..19,z=-34..11".to_string(),
            "off x=35..47,y=-46..-34,z=-11..5".to_string(),
            "on x=-14..36,y=-6..44,z=-16..29".to_string(),
            "on x=-57795..-6158,y=29564..72030,z=20435..90618".to_string(),
            "on x=36731..105352,y=-21140..28532,z=16094..90401".to_string(),
            "on x=30999..107136,y=-53464..15513,z=8553..71215".to_string(),
            "on x=13528..83982,y=-99403..-27377,z=-24141..23996".to_string(),
            "on x=-72682..-12347,y=18159..111354,z=7391..80950".to_string(),
            "on x=-1060..80757,y=-65301..-20884,z=-103788..-16709".to_string(),
            "on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856".to_string(),
            "on x=-52752..22273,y=-49450..9096,z=54442..119054".to_string(),
            "on x=-29982..40483,y=-108474..-28371,z=-24328..38471".to_string(),
            "on x=-4958..62750,y=40422..118853,z=-7672..65583".to_string(),
            "on x=55694..108686,y=-43367..46958,z=-26781..48729".to_string(),
            "on x=-98497..-18186,y=-63569..3412,z=1232..88485".to_string(),
            "on x=-726..56291,y=-62629..13224,z=18033..85226".to_string(),
            "on x=-110886..-34664,y=-81338..-8658,z=8914..63723".to_string(),
            "on x=-55829..24974,y=-16897..54165,z=-121762..-28058".to_string(),
            "on x=-65152..-11147,y=22489..91432,z=-58782..1780".to_string(),
            "on x=-120100..-32970,y=-46592..27473,z=-11695..61039".to_string(),
            "on x=-18631..37533,y=-124565..-50804,z=-35667..28308".to_string(),
            "on x=-57817..18248,y=49321..117703,z=5745..55881".to_string(),
            "on x=14781..98692,y=-1341..70827,z=15753..70151".to_string(),
            "on x=-34419..55919,y=-19626..40991,z=39015..114138".to_string(),
            "on x=-60785..11593,y=-56135..2999,z=-95368..-26915".to_string(),
            "on x=-32178..58085,y=17647..101866,z=-91405..-8878".to_string(),
            "on x=-53655..12091,y=50097..105568,z=-75335..-4862".to_string(),
            "on x=-111166..-40997,y=-71714..2688,z=5609..50954".to_string(),
            "on x=-16602..70118,y=-98693..-44401,z=5197..76897".to_string(),
            "on x=16383..101554,y=4615..83635,z=-44907..18747".to_string(),
            "off x=-95822..-15171,y=-19987..48940,z=10804..104439".to_string(),
            "on x=-89813..-14614,y=16069..88491,z=-3297..45228".to_string(),
            "on x=41075..99376,y=-20427..49978,z=-52012..13762".to_string(),
            "on x=-21330..50085,y=-17944..62733,z=-112280..-30197".to_string(),
            "on x=-16478..35915,y=36008..118594,z=-7885..47086".to_string(),
            "off x=-98156..-27851,y=-49952..43171,z=-99005..-8456".to_string(),
            "off x=2032..69770,y=-71013..4824,z=7471..94418".to_string(),
            "on x=43670..120875,y=-42068..12382,z=-24787..38892".to_string(),
            "off x=37514..111226,y=-45862..25743,z=-16714..54663".to_string(),
            "off x=25699..97951,y=-30668..59918,z=-15349..69697".to_string(),
            "off x=-44271..17935,y=-9516..60759,z=49131..112598".to_string(),
            "on x=-61695..-5813,y=40978..94975,z=8655..80240".to_string(),
            "off x=-101086..-9439,y=-7088..67543,z=33935..83858".to_string(),
            "off x=18020..114017,y=-48931..32606,z=21474..89843".to_string(),
            "off x=-77139..10506,y=-89994..-18797,z=-80..59318".to_string(),
            "off x=8476..79288,y=-75520..11602,z=-96624..-24783".to_string(),
            "on x=-47488..-1262,y=24338..100707,z=16292..72967".to_string(),
            "off x=-84341..13987,y=2429..92914,z=-90671..-1318".to_string(),
            "off x=-37810..49457,y=-71013..-7894,z=-105357..-13188".to_string(),
            "off x=-27365..46395,y=31009..98017,z=15428..76570".to_string(),
            "off x=-70369..-16548,y=22648..78696,z=-1892..86821".to_string(),
            "on x=-53470..21291,y=-120233..-33476,z=-44150..38147".to_string(),
            "off x=-93533..-4276,y=-16170..68771,z=-104985..-24507".to_string(),
        ];

        let mut r = Reactor::from_lines(&l);
        assert_eq!(r.start(false), 2758514936282235);
    }
}
