//! --- Day 9: Smoke Basin ---
//!
//! These caves seem to be lava tubes. Parts are even still volcanically active; small hydrothermal vents release smoke into the caves that slowly settles like rain.
//!
//! If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer. The submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).
//!
//! Smoke flows to the lowest point of the area it's in. For example, consider the following heightmap:
//!
//! 2199943210
//! 3987894921
//! 9856789892
//! 8767896789
//! 9899965678
//!
//! Each number corresponds to the height of a particular location, where 9 is the highest and 0 is the lowest a location can be.
//!
//! Your first goal is to find the low points - the locations that are lower than any of its adjacent locations. Most locations have four adjacent locations (up, down, left, and right); locations on the edge or corner of the map have three or two adjacent locations, respectively. (Diagonal locations do not count as adjacent.)
//!
//! In the above example, there are four low points, all highlighted: two are in the first row (a 1 and a 0), one is in the third row (a 5), and one is in the bottom row (also a 5). All other locations on the heightmap have some lower adjacent location, and so are not low points.
//!
//! The risk level of a low point is 1 plus its height. In the above example, the risk levels of the low points are 2, 1, 6, and 6. The sum of the risk levels of all low points in the heightmap is therefore 15.
//!
//! Find all of the low points on your heightmap. What is the sum of the risk levels of all low points on your heightmap?
//!
//!--- Part Two ---
//!
//! Next, you need to find the largest basins so you know what areas are most important to avoid.
//!
//! A basin is all locations that eventually flow downward to a single low point. Therefore, every low point has a basin, although some basins are very small. Locations of height 9 do not count as being in any basin, and all other locations will always be part of exactly one basin.
//!
//! The size of a basin is the number of locations within the basin, including the low point. The example above has four basins.
//!
//! The top-left basin, size 3:
//!
//! 2199943210
//! 3987894921
//! 9856789892
//! 8767896789
//! 9899965678
//!
//! The top-right basin, size 9:
//!
//! 2199943210
//! 3987894921
//! 9856789892
//! 8767896789
//! 9899965678
//!
//! The middle basin, size 14:
//!
//! 2199943210
//! 3987894921
//! 9856789892
//! 8767896789
//! 9899965678
//!
//! The bottom-right basin, size 9:
//!
//! 2199943210
//! 3987894921
//! 9856789892
//! 8767896789
//! 9899965678
//!
//! Find the three largest basins and multiply their sizes together. In the above example, this is 9 * 14 * 9 = 1134.
//!
//! What do you get if you multiply together the sizes of the three largest basins?
//!

use std::collections::HashMap;

fn main() {
    let heightmap = advent_of_code_2021::input_file("input/day8.input").unwrap();
    let mut h = HashMap::new();
    map(heightmap, &mut h);
    let low = collect_low(&h);
    let risk = calc_risk(&low);
    println!("Risk Sum: {}", risk);
    let size_product = calc_basin_product(&h);
    println!("Basin Size Product: {}", size_product);
}

fn map(raw: Vec<String>, map: &mut HashMap<(i32, i32), u32>) {
    for (i, line) in raw.iter().enumerate() {
        for (j, c) in line.chars().enumerate() {
            map.insert((i as i32, j as i32), c.to_digit(10).unwrap());
        }
    }
}

fn check_low(i: i32, j: i32, map: &HashMap<(i32, i32), u32>) -> Option<u32> {
    if let Some(here) = map.get(&(i, j)) {
        if let Some(north) = map.get(&(i - 1, j)) {
            if north <= here {
                return None;
            }
        }
        if let Some(south) = map.get(&(i + 1, j)) {
            if south <= here {
                return None;
            }
        }
        if let Some(east) = map.get(&(i, j - 1)) {
            if east <= here {
                return None;
            }
        }
        if let Some(west) = map.get(&(i, j + 1)) {
            if west <= here {
                return None;
            }
        }
        return Some(*here);
    }
    None
}

fn collect_low(map: &HashMap<(i32, i32), u32>) -> Vec<u32> {
    let mut r = Vec::new();
    for (key, _val) in map.iter() {
        let (i, j) = key;
        if let Some(low) = check_low(*i, *j, map) {
            r.push(low);
        }
    }
    r
}

fn calc_risk(list: &Vec<u32>) -> u32 {
    list.iter().map(|x| x + 1).sum()
}

fn calc_basin_size(
    map: &HashMap<(i32, i32), u32>,
    loc: (i32, i32),
    prev: &mut Vec<(i32, i32)>,
) -> u32 {
    let mut sum = 1;
    prev.push(loc);
    if let Some(here) = map.get(&loc) {
        if *here == 9 {
            0
        } else {
            let (i, j) = loc;

            let next_list = [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)];

            'm: for next in next_list.iter() {
                for p in prev.iter() {
                    if *p == *next {
                        continue 'm;
                    }
                }

                sum += calc_basin_size(map, *next, prev);
            }

            sum
        }
    } else {
        0
    }
}

fn list_low(map: &HashMap<(i32, i32), u32>) -> Vec<(i32, i32)> {
    let mut r = Vec::new();
    for (key, _val) in map.iter() {
        let (i, j) = key;
        if let Some(_low) = check_low(*i, *j, map) {
            r.push(*key);
        }
    }
    r
}

fn find_basin_sizes(map: &HashMap<(i32, i32), u32>, low: Vec<(i32, i32)>) -> Vec<u32> {
    let mut r = Vec::new();
    let mut p = Vec::new();
    for l in low.iter() {
        r.push(calc_basin_size(map, *l, &mut p));
    }
    r
}

fn calc_basin_product(map: &HashMap<(i32, i32), u32>) -> u32 {
    let low = list_low(map);
    let mut size = find_basin_sizes(map, low);
    size.sort();
    size.reverse();
    println!("Sizes: {:?}", &size[0..3]);
    let mut p = 1;
    for s in (&size[0..3]).iter() {
        p *= *s;
    }
    p
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc_basin_product() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let size_product = calc_basin_product(&h);
        assert_eq!(size_product, 1134);
    }

    #[test]
    fn test_find_basin_sizes() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let low = list_low(&h);
        let mut s = find_basin_sizes(&h, low);
        s.sort();
        assert_eq!(s, vec![3, 9, 9, 14]);
    }

    #[test]
    fn test_list_low() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut low = list_low(&h);
        low.sort();
        assert_eq!(low, vec![(0, 1), (0, 9), (2, 2), (4, 6),]);
    }

    #[test]
    fn test_calc_basin_size_test1() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (0, 1), &mut p), 3);
    }

    #[test]
    fn test_calc_basin_size_test2() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (0, 9), &mut p), 9);
    }

    #[test]
    fn test_calc_basin_size_test3() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (2, 2), &mut p), 14);
    }

    #[test]
    fn test_calc_basin_size_test4() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (4, 6), &mut p), 9);
    }

    #[test]
    fn test_calc_basin_size_peak() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (0, 2), &mut p), 0);
    }

    #[test]
    fn test_calc_basin_size_missing() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut p = Vec::new();
        assert_eq!(calc_basin_size(&h, (0, 11), &mut p), 0);
    }

    #[test]
    fn test_calc_risk() {
        assert_eq!(calc_risk(&vec![0, 1, 5, 5]), 15);
    }

    #[test]
    fn test_collec_low() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let mut low = collect_low(&h);
        low.sort();
        assert_eq!(low, vec![0, 1, 5, 5]);
    }

    #[test]
    fn test_low_false() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let v = check_low(1, 9, &h);
        assert_eq!(v, None);
    }

    #[test]
    fn test_low_true() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        let v = check_low(0, 9, &h);
        assert_eq!(v.unwrap(), 0);
    }

    #[test]
    fn test_map() {
        let raw = vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ];
        let mut h = HashMap::new();
        map(raw, &mut h);
        println!("Map: {:?}", h);
    }
}
