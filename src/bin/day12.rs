//! --- Day 13: Transparent Origami ---
//!
//! You reach another volcanically active part of the cave. It would be nice if you could do some kind of thermal imaging so you could tell ahead of time which caves are too hot to safely enter.
//!
//! Fortunately, the submarine seems to be equipped with a thermal camera! When you activate it, you are greeted with:
//!
//! Congratulations on your purchase! To activate this infrared thermal imaging
//! camera system, please enter the code found on page 1 of the manual.
//!
//! Apparently, the Elves have never used this feature. To your surprise, you manage to find the manual; as you go to open it, page 1 falls out. It's a large sheet of transparent paper! The transparent paper is marked with random dots and includes instructions on how to fold it up (your puzzle input). For example:
//!
//! 6,10
//! 0,14
//! 9,10
//! 0,3
//! 10,4
//! 4,11
//! 6,0
//! 6,12
//! 4,1
//! 0,13
//! 10,12
//! 3,4
//! 3,0
//! 8,4
//! 1,10
//! 2,14
//! 8,10
//! 9,0
//!
//! fold along y=7
//! fold along x=5
//!
//! The first section is a list of dots on the transparent paper. 0,0 represents the top-left coordinate. The first value, x, increases to the right. The second value, y, increases downward. So, the coordinate 3,0 is to the right of 0,0, and the coordinate 0,7 is below 0,0. The coordinates in this example form the following pattern, where # is a dot on the paper and . is an empty, unmarked position:
//!
//! ...#..#..#.
//! ....#......
//! ...........
//! #..........
//! ...#....#.#
//! ...........
//! ...........
//! ...........
//! ...........
//! ...........
//! .#....#.##.
//! ....#......
//! ......#...#
//! #..........
//! #.#........
//!
//! Then, there is a list of fold instructions. Each instruction indicates a line on the transparent paper and wants you to fold the paper up (for horizontal y=... lines) or left (for vertical x=... lines). In this example, the first fold instruction is fold along y=7, which designates the line formed by all of the positions where y is 7 (marked here with -):
//!
//! ...#..#..#.
//! ....#......
//! ...........
//! #..........
//! ...#....#.#
//! ...........
//! ...........
//! -----------
//! ...........
//! ...........
//! .#....#.##.
//! ....#......
//! ......#...#
//! #..........
//! #.#........
//!
//! Because this is a horizontal line, fold the bottom half up. Some of the dots might end up overlapping after the fold is complete, but dots will never appear exactly on a fold line. The result of doing this fold looks like this:
//!
//! #.##..#..#.
//! #...#......
//! ......#...#
//! #...#......
//! .#.#..#.###
//! ...........
//! ...........
//!
//! Now, only 17 dots are visible.
//!
//! Notice, for example, the two dots in the bottom left corner before the transparent paper is folded; after the fold is complete, those dots appear in the top left corner (at 0,0 and 0,1). Because the paper is transparent, the dot just below them in the result (at 0,3) remains visible, as it can be seen through the transparent paper.
//!
//! Also notice that some dots can end up overlapping; in this case, the dots merge together and become a single dot.
//!
//! The second fold instruction is fold along x=5, which indicates this line:
//!
//! #.##.|#..#.
//! #...#|.....
//! .....|#...#
//! #...#|.....
//! .#.#.|#.###
//! .....|.....
//! .....|.....
//!
//! Because this is a vertical line, fold left:
//!
//! #####
//! #...#
//! #...#
//! #...#
//! #####
//! .....
//! .....
//!
//! The instructions made a square!
//!
//! The transparent paper is pretty big, so for now, focus on just completing the first fold. After the first fold in the example above, 17 dots are visible - dots that end up overlapping after the fold is completed count as a single dot.
//!
//! How many dots are visible after completing just the first fold instruction on your transparent paper?

use std::collections::HashSet;
use std::fmt;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day12.input").unwrap();
    let mut parts = lines.split(|l| l == "");
    let mut s = Sheet::from_lines(&parts.next().unwrap());
    let f = folds_from_lines(&parts.next().unwrap());
    let mut fold_iter = f.iter();
    s.fold(*fold_iter.next().unwrap());
    println!("Dots after first fold: {}", s.dots.len());
    for f in fold_iter {
        s.fold(*f);
    }
    println!("Dots after last fold: {}", s.dots.len());
    println!("Sheet: {}", s);
}

#[derive(Debug, Clone, Copy)]
enum Fold {
    X(i32),
    Y(i32),
}

impl Fold {
    pub fn from_string(line: &str) -> Fold {
        let mut s = line.split("=");
        match s.next().unwrap().chars().last().unwrap() {
            'x' => Fold::X(s.next().unwrap().parse().unwrap()),
            'y' => Fold::Y(s.next().unwrap().parse().unwrap()),
            _ => panic!("Invalid fold."),
        }
    }
}

fn folds_from_lines(lines: &[String]) -> Vec<Fold> {
    let mut r = Vec::new();
    for l in lines.iter() {
        r.push(Fold::from_string(l));
    }
    r
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Pos {
    x: i32,
    y: i32,
}

impl Pos {
    pub fn from_string(line: &str) -> Pos {
        let mut s = line.split(",");
        let x = s.next().unwrap().parse().unwrap();
        let y = s.next().unwrap().parse().unwrap();
        Pos { x: x, y: y }
    }
}

#[derive(Debug)]
struct Sheet {
    dots: HashSet<Pos>,
    max_x: i32,
    max_y: i32,
}

impl Sheet {
    pub fn from_lines(lines: &[String]) -> Sheet {
        let mut s = Sheet {
            dots: HashSet::new(),
            max_x: 0,
            max_y: 0,
        };

        for l in lines.iter() {
            let p = Pos::from_string(l);

            if p.x > s.max_x {
                s.max_x = p.x;
            }
            if p.y > s.max_y {
                s.max_y = p.y;
            }

            s.dots.insert(p);
        }

        s
    }

    fn fold_y(&mut self, fold_y: i32) {
        let temp_dots = self.dots.clone();

        for p in temp_dots.iter().filter(|d| d.y == fold_y) {
            self.dots.remove(p);
        }

        for p in temp_dots.iter().filter(|d| d.y > fold_y) {
            let mut new_p = p.clone();
            new_p.y = 2 * fold_y - new_p.y;
            self.dots.insert(new_p);
            self.dots.remove(p);
        }

        self.max_y = fold_y - 1;
    }

    fn fold_x(&mut self, fold_x: i32) {
        let temp_dots = self.dots.clone();

        for p in temp_dots.iter().filter(|d| d.x == fold_x) {
            self.dots.remove(p);
        }

        for p in temp_dots.iter().filter(|d| d.x > fold_x) {
            let mut new_p = p.clone();
            new_p.x = 2 * fold_x - new_p.x;
            self.dots.insert(new_p);
            self.dots.remove(p);
        }

        self.max_x = fold_x - 1;
    }

    pub fn fold(&mut self, fold: Fold) {
        match fold {
            Fold::X(x) => self.fold_x(x),
            Fold::Y(y) => self.fold_y(y),
        }
    }
}

impl fmt::Display for Sheet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut out = String::new();

        let mut v = Vec::new();
        let mut t = Vec::new();
        t.resize((self.max_x + 1).try_into().unwrap(), '.');
        for _i in 0..self.max_y + 1 {
            v.push(t.clone());
        }

        for p in self.dots.iter() {
            v[p.y as usize][p.x as usize] = 'x';
        }

        for i in v.iter() {
            let s: String = i.into_iter().collect();
            out.push_str(&s);
            out.push('\n');
        }
        write!(f, "\n{}", out)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pos_new() {
        let p = Pos::from_string(&"4,5");
        assert_eq!(p.x, 4);
        assert_eq!(p.y, 5);
    }

    #[test]
    fn test_sheet_new() {
        let lines = vec![
            "6,10".to_string(),
            "0,14".to_string(),
            "9,10".to_string(),
            "0,3".to_string(),
            "10,4".to_string(),
            "4,11".to_string(),
            "6,0".to_string(),
            "6,12".to_string(),
            "4,1".to_string(),
            "0,13".to_string(),
            "10,12".to_string(),
            "3,4".to_string(),
            "3,0".to_string(),
            "8,4".to_string(),
            "1,10".to_string(),
            "2,14".to_string(),
            "8,10".to_string(),
            "9,0".to_string(),
        ];
        let s = Sheet::from_lines(&lines);
        println!("Sheet: {}", s);
        assert_eq!(s.max_x, 10);
        assert_eq!(s.max_y, 14);
    }

    #[test]
    fn test_sheet_fold_y() {
        let lines = vec![
            "6,10".to_string(),
            "0,14".to_string(),
            "9,10".to_string(),
            "0,3".to_string(),
            "10,4".to_string(),
            "4,11".to_string(),
            "6,0".to_string(),
            "6,12".to_string(),
            "4,1".to_string(),
            "0,13".to_string(),
            "10,12".to_string(),
            "3,4".to_string(),
            "3,0".to_string(),
            "8,4".to_string(),
            "1,10".to_string(),
            "2,14".to_string(),
            "8,10".to_string(),
            "9,0".to_string(),
        ];
        let mut s = Sheet::from_lines(&lines);
        s.fold_y(7);
        assert_eq!(s.dots.len(), 17);
    }

    #[test]
    fn test_sheet_fold_x() {
        let lines = vec![
            "6,10".to_string(),
            "0,14".to_string(),
            "9,10".to_string(),
            "0,3".to_string(),
            "10,4".to_string(),
            "4,11".to_string(),
            "6,0".to_string(),
            "6,12".to_string(),
            "4,1".to_string(),
            "0,13".to_string(),
            "10,12".to_string(),
            "3,4".to_string(),
            "3,0".to_string(),
            "8,4".to_string(),
            "1,10".to_string(),
            "2,14".to_string(),
            "8,10".to_string(),
            "9,0".to_string(),
        ];
        let mut s = Sheet::from_lines(&lines);
        s.fold_y(7);
        s.fold_x(5);
        println!("Sheet: {}", s);
        assert_eq!(s.dots.len(), 16);
    }
}
