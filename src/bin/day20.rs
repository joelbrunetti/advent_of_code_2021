//! --- Day 21: Dirac Dice ---
//!
//! There's not much to do as you slowly descend to the bottom of the ocean. The submarine computer challenges you to a nice game of Dirac Dice.
//!
//! This game consists of a single die, two pawns, and a game board with a circular track containing ten spaces marked 1 through 10 clockwise. Each player's starting space is chosen randomly (your puzzle input). Player 1 goes first.
//!
//! Players take turns moving. On each player's turn, the player rolls the die three times and adds up the results. Then, the player moves their pawn that many times forward around the track (that is, moving clockwise on spaces in order of increasing value, wrapping back around to 1 after 10). So, if a player is on space 7 and they roll 2, 2, and 1, they would move forward 5 times, to spaces 8, 9, 10, 1, and finally stopping on 2.
//!
//! After each player moves, they increase their score by the value of the space their pawn stopped on. Players' scores start at 0. So, if the first player starts on space 7 and rolls a total of 5, they would stop on space 2 and add 2 to their score (for a total score of 2). The game immediately ends as a win for any player whose score reaches at least 1000.
//!
//! Since the first game is a practice game, the submarine opens a compartment labeled deterministic dice and a 100-sided die falls out. This die always rolls 1 first, then 2, then 3, and so on up to 100, after which it starts over at 1 again. Play using this die.
//!
//! For example, given these starting positions:
//!
//! Player 1 starting position: 4
//! Player 2 starting position: 8
//!
//! This is how the game would go:
//!
//!     Player 1 rolls 1+2+3 and moves to space 10 for a total score of 10.
//!     Player 2 rolls 4+5+6 and moves to space 3 for a total score of 3.
//!     Player 1 rolls 7+8+9 and moves to space 4 for a total score of 14.
//!     Player 2 rolls 10+11+12 and moves to space 6 for a total score of 9.
//!     Player 1 rolls 13+14+15 and moves to space 6 for a total score of 20.
//!     Player 2 rolls 16+17+18 and moves to space 7 for a total score of 16.
//!     Player 1 rolls 19+20+21 and moves to space 6 for a total score of 26.
//!     Player 2 rolls 22+23+24 and moves to space 6 for a total score of 22.
//!
//! ...after many turns...
//!
//!     Player 2 rolls 82+83+84 and moves to space 6 for a total score of 742.
//!     Player 1 rolls 85+86+87 and moves to space 4 for a total score of 990.
//!     Player 2 rolls 88+89+90 and moves to space 3 for a total score of 745.
//!     Player 1 rolls 91+92+93 and moves to space 10 for a final score, 1000.
//!
//! Since player 1 has at least 1000 points, player 1 wins and the game ends. At this point, the losing player had 745 points and the die had been rolled a total of 993 times; 745 * 993 = 739785.
//!
//! Play a practice game using the deterministic 100-sided die. The moment either player wins, what do you get if you multiply the score of the losing player by the number of times the die was rolled during the game?
//!
//!--- Part Two ---
//!
//! Now that you're warmed up, it's time to play the real game.
//!
//! A second compartment opens, this time labeled Dirac dice. Out of it falls a single three-sided die.
//!
//! As you experiment with the die, you feel a little strange. An informational brochure in the compartment explains that this is a quantum die: when you roll it, the universe splits into multiple copies, one copy for each possible outcome of the die. In this case, rolling the die always splits the universe into three copies: one where the outcome of the roll was 1, one where it was 2, and one where it was 3.
//!
//! The game is played the same as before, although to prevent things from getting too far out of hand, the game now ends when either player's score reaches at least 21.
//!
//! Using the same starting positions as in the example above, player 1 wins in 444356092776315 universes, while player 2 merely wins in 341960390180808 universes.
//!
//! Using your given starting positions, determine every possible outcome. Find the player that wins in more universes; in how many universes does that player win?
//!

use std::collections::HashMap;

fn main() {
    let mut p: Vec<Player> = vec![9, 6].iter().map(|pos| Player::new(*pos)).collect();
    let mut g = Game::new();
    g.play(&mut p);
    println!("Easy Game Final score: {}", g.score(&p));

    let dg = DiracGame::new(3, 21, 9, 6);
    let (p1_wins, p2_wins) = dg.count_games();
    let winner = std::cmp::max(p1_wins, p2_wins);
    println!("Dirac Game Play Wins: {}", winner);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Player {
    score: u32,
    pos: u32,
}

impl Player {
    pub fn new(pos: u32) -> Player {
        Player { score: 0, pos: pos }
    }

    pub fn play(&mut self, roll: u32) -> bool {
        self.pos = (roll + self.pos - 1) % 10 + 1;
        if self.pos == 0 {
            panic!("Position 0 does not exist!")
        };

        self.score += self.pos;

        if self.score < 1000 {
            false
        } else {
            true
        }
    }
}

struct Game {
    count: u32,
    current: u32,
}

impl Game {
    pub fn new() -> Game {
        Game {
            count: 0,
            current: 1,
        }
    }

    fn roll(&mut self) -> u32 {
        let mut val = 0;
        for _i in 0..3 {
            val += self.current;
            if self.current < 100 {
                self.current += 1;
            } else {
                self.current = 1;
            }
            self.count += 1;
        }
        val
    }

    pub fn play(&mut self, players: &mut Vec<Player>) {
        'g: loop {
            for (_i, p) in players.iter_mut().enumerate() {
                let r = self.roll();

                if p.play(r) {
                    break 'g;
                }
            }
        }
    }

    pub fn score(&self, players: &Vec<Player>) -> u32 {
        let mut loser = u32::MAX;
        for p in players.iter() {
            if p.score < loser {
                loser = p.score
            }
        }
        loser * self.count
    }
}

struct DiracGame {
    die_rolls: HashMap<u32, u64>,
    winning: u32,
    p1: Player,
    p2: Player,
}

impl DiracGame {
    pub fn new(sides: u32, winning: u32, p1_start: u32, p2_start: u32) -> DiracGame {
        DiracGame {
            die_rolls: DiracGame::calc_die_rolls(sides),
            winning: winning,
            p1: Player::new(p1_start),
            p2: Player::new(p2_start),
        }
    }

    fn calc_die_rolls(sides: u32) -> HashMap<u32, u64> {
        let mut perms = HashMap::new();
        for a in 1..=sides {
            for b in 1..=sides {
                for c in 1..=sides {
                    let count = perms.entry(a + b + c).or_insert(0);
                    *count += 1;
                }
            }
        }

        perms
    }

    pub fn count_games(&self) -> (u64, u64) {
        let mut cache: HashMap<(Player, Player, bool), (u64, u64)> = HashMap::new();
        self.count_games_recur(self.p1, self.p2, true, &mut cache)
    }

    pub fn count_games_recur(
        &self,
        p1: Player,
        p2: Player,
        p1_turn: bool,
        cache: &mut HashMap<(Player, Player, bool), (u64, u64)>,
    ) -> (u64, u64) {
        match cache.get(&(p1, p2, p1_turn)) {
            Some(result) => *result,
            None => {
                if p1.score >= self.winning {
                    return (1, 0);
                }

                if p2.score >= self.winning {
                    return (0, 1);
                }

                let mut p1_wins: u64 = 0;
                let mut p2_wins: u64 = 0;

                for (roll, freq) in self.die_rolls.iter() {
                    let mut p1_next = p1;
                    let mut p2_next = p2;

                    if p1_turn {
                        p1_next.play(*roll);
                    } else {
                        p2_next.play(*roll);
                    }

                    let (p1_win, p2_win) =
                        self.count_games_recur(p1_next, p2_next, !p1_turn, cache);

                    p1_wins += p1_win * *freq as u64;
                    p2_wins += p2_win * *freq as u64;
                }

                cache.insert((p1, p2, p1_turn), (p1_wins, p2_wins));
                (p1_wins, p2_wins)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_diracgame_new() {
        let dg = DiracGame::new(3, 21, 4, 8);
        for (k, v) in dg.die_rolls.iter() {
            println!("Value: {}, Count: {}", k, v);
        }
    }

    #[test]
    fn test_diracgame_count() {
        let dg = DiracGame::new(3, 21, 4, 8);
        let (p1_wins, p2_wins) = dg.count_games();
        assert_eq!(p1_wins, 444356092776315);
        assert_eq!(p2_wins, 341960390180808);
    }

    #[test]
    fn test_player_play() {
        let mut p = Player::new(4);
        assert_eq!(p.score, 0);
        assert_eq!(p.pos, 4);
        let m = p.play(6);
        assert_eq!(m, false);
        assert_eq!(p.score, 10);
        assert_eq!(p.pos, 10);
        let m = p.play(24);
        assert_eq!(m, false);
        assert_eq!(p.score, 14);
        assert_eq!(p.pos, 4);
        let m = p.play(14 * 3);
        assert_eq!(m, false);
        assert_eq!(p.score, 20);
        assert_eq!(p.pos, 6);
        p.score = 998;
        let m = p.play(2);
        assert_eq!(m, true);
        assert_eq!(p.score, 1006);
        assert_eq!(p.pos, 8);
    }

    #[test]
    fn test_game_new() {
        let g = Game::new();
        assert_eq!(g.count, 0);
        assert_eq!(g.current, 1);
    }

    #[test]
    fn test_game_play() {
        let mut p: Vec<Player> = vec![4, 8].iter().map(|pos| Player::new(*pos)).collect();
        let mut g = Game::new();
        assert_eq!(g.count, 0);
        assert_eq!(g.current, 1);
        assert_eq!(p[0].pos, 4);
        assert_eq!(p[1].pos, 8);
        g.play(&mut p);
        assert_eq!(g.count, 993);
        assert_eq!(g.current, 94);
        assert_eq!(p[0].score, 1000);
        assert_eq!(p[1].score, 745);
        assert_eq!(g.score(&p), 739785);
    }

    #[test]
    fn test_game_roll() {
        let mut g = Game {
            count: 0,
            current: 1,
        };
        assert_eq!(g.count, 0);
        assert_eq!(g.current, 1);
        assert_eq!(g.roll(), 6);
        assert_eq!(g.count, 3);
        assert_eq!(g.current, 4);
        assert_eq!(g.roll(), 15);
        assert_eq!(g.roll(), 24);
        g.current = 99;
        assert_eq!(g.roll(), 99 + 100 + 1);
    }
}
