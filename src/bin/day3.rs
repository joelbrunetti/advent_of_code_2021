//! --- Day 4: Giant Squid ---
//!
//! You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that you can't see any sunlight. What you can see, however, is a giant squid that has attached itself to the outside of your submarine.
//!
//! Maybe it wants to play bingo?
//!
//! Bingo is played on a set of boards each consisting of a 5x5 grid of numbers. Numbers are chosen at random, and the chosen number is marked on all boards on which it appears. (Numbers may not appear on all boards.) If all numbers in any row or any column of a board are marked, that board wins. (Diagonals don't count.)
//!
//! The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass the time. It automatically generates a random order in which to draw numbers and a random set of boards (your puzzle input). For example:
//!
//! 7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
//!
//! 22 13 17 11  0
//!  8  2 23  4 24
//! 21  9 14 16  7
//!  6 10  3 18  5
//!  1 12 20 15 19
//!
//!  3 15  0  2 22
//!  9 18 13 17  5
//! 19  8  7 25 23
//! 20 11 10 24  4
//! 14 21 16 12  6
//!
//! 14 21 17 24  4
//! 10 16 15  9 19
//! 18  8 23 26 20
//! 22 11 13  6  5
//!  2  0 12  3  7
//!
//! After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners, but the boards are marked as follows (shown here adjacent to each other to save space):
//!
//! 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//!  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
//! 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//!  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//!  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
//!
//! After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:
//!
//! 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//!  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
//! 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//!  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//!  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
//!
//! Finally, 24 is drawn:
//!
//! 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//!  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
//! 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//!  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//!  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
//!
//! At this point, the third board wins because it has at least one complete row or column of marked numbers (in this case, the entire top row is marked: 14 21 17 24 4).
//!
//! The score of the winning board can now be calculated. Start by finding the sum of all unmarked numbers on that board; in this case, the sum is 188. Then, multiply that sum by the number that was just called when the board won, 24, to get the final score, 188 * 24 = 4512.
//!
//! To guarantee victory against the giant squid, figure out which board will win first. What will your final score be if you choose that board?
//!
//!--- Part Two ---
//!
//! On the other hand, it might be wise to try a different strategy: let the giant squid win.
//!
//! You aren't sure how many bingo boards a giant squid could play at once, so rather than waste time counting its arms, the safe thing to do is to figure out which board will win last and choose that one. That way, no matter which boards it picks, it will win for sure.
//!
//! In the above example, the second board is the last to win, which happens after 13 is eventually called and its middle column is completely marked. If you were to keep playing until this point, the second board would have a sum of unmarked numbers equal to 148 for a final score of 148 * 13 = 1924.
//!
//! Figure out which board will win last. Once it wins, what would its final score be?
//!

use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let file = input_file("input/day3.input").unwrap();
    let calls = parse_calls(&file[0]).unwrap();
    let mut boards = parse_boards(&file[2..]).unwrap();
    println!(
        "Winning Score: {}",
        find_winner(&calls, &mut boards).unwrap()
    );
    println!("Losing Score: {}", find_loser(&calls, &mut boards).unwrap());
}

fn find_winner(calls: &[i32], boards: &mut [Board]) -> Option<i32> {
    let mut win_rank = calls.len() as i32;
    let mut win_score = None;

    for b in boards {
        match b.play(&calls[..win_rank as usize]) {
            Some(r) => {
                if r < win_rank {
                    win_rank = r;
                    win_score = b.score();
                }
            }
            None => {}
        }
    }

    win_score
}

fn find_loser(calls: &[i32], boards: &mut [Board]) -> Option<i32> {
    let mut lose_rank = 0;
    let mut lose_score = None;

    for b in boards {
        match b.play(calls) {
            Some(r) => {
                if r > lose_rank {
                    lose_rank = r;
                    lose_score = b.score();
                }
            }
            None => {}
        }
    }

    lose_score
}

fn input_file(filename: &str) -> Result<Vec<String>, Box<dyn Error>> {
    let path = Path::new(filename);
    let file = File::open(&path)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

fn parse_calls(calls: &str) -> Result<Vec<i32>, Box<dyn Error>> {
    Ok(calls
        .split(",")
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<i32>>())
}

fn parse_boards(input: &[String]) -> Result<Vec<Board>, Box<dyn Error>> {
    let mut boards = Vec::new();

    for b in input.split(|s| s == "") {
        let mut numbers = Vec::<i32>::new();
        for l in b {
            for n in l.split_whitespace() {
                numbers.push(n.parse::<i32>().unwrap());
            }
        }
        boards.push(Board::new(&numbers[..]));
    }

    Ok(boards)
}

#[derive(Debug, Copy, Clone)]
struct Number {
    v: i32,
    called: bool,
}

#[derive(Debug)]
struct Board {
    win_rank: Option<i32>,
    win_call: Option<i32>,
    row: [[Number; 5]; 5],
}

impl Board {
    pub fn new(nums: &[i32]) -> Board {
        let mut b = Board {
            win_rank: None,
            win_call: None,
            row: [[Number {
                v: 0,
                called: false,
            }; 5]; 5],
        };

        for (i, n) in nums.iter().enumerate() {
            b.row[i / 5][i % 5].v = *n;
        }

        b
    }

    fn call_num(&mut self, num: i32) {
        for col in self.row.iter_mut() {
            for n in col.iter_mut() {
                if n.v == num {
                    n.called = true;
                }
            }
        }
    }

    fn check_rows(&self) -> bool {
        for col in self.row.iter() {
            let mut win = true;
            for n in col.iter() {
                win = win && n.called;
            }
            if win {
                return true;
            }
        }
        false
    }

    fn check_cols(&self) -> bool {
        let mut win_arr: [bool; 5] = [true; 5];
        for col in self.row.iter() {
            for (n, win) in col.iter().zip(win_arr.iter_mut()) {
                *win = *win && n.called;
            }
        }

        for win in win_arr.iter() {
            if *win {
                return true;
            }
        }

        false
    }

    pub fn play(&mut self, play_calls: &[i32]) -> Option<i32> {
        for (i, play) in play_calls.iter().enumerate() {
            self.call_num(*play);
            if self.check_rows() || self.check_cols() {
                self.win_rank = Some(i as i32);
                self.win_call = Some(*play);
                return Some(i as i32);
            }
        }

        None
    }

    pub fn score(&self) -> Option<i32> {
        match self.win_call {
            Some(x) => {
                let mut sum_uncalled = 0;
                for col in self.row.iter() {
                    for n in col.iter() {
                        if !n.called {
                            sum_uncalled += n.v;
                        }
                    }
                }
                Some(x * sum_uncalled)
            }
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_PLAY_CALLS: [i32; 27] = [
        7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3,
        26, 1,
    ];

    const TEST_BOARD: [i32; 25] = [
        14, 21, 17, 24, 4, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, 7,
    ];

    #[test]
    fn test_bingo_board_new() {
        let b = Board::new(&TEST_BOARD);
        for (i, col) in b.row.iter().enumerate() {
            println!("Row {}: {:?}", i, col);
        }
    }

    #[test]
    fn test_bingo_board_call_num() {
        let mut b = Board::new(&TEST_BOARD);
        b.call_num(17);
        assert_eq!(b.row[0][2].called, true);
    }

    #[test]
    fn test_bingo_board_check_rows() {
        let mut b = Board::new(&TEST_BOARD);
        b.call_num(18);
        b.call_num(8);
        b.call_num(23);
        b.call_num(26);
        b.call_num(20);
        assert!(b.check_rows())
    }

    #[test]
    fn test_bingo_board_check_cols() {
        let mut b = Board::new(&TEST_BOARD);
        b.call_num(17);
        b.call_num(15);
        b.call_num(23);
        b.call_num(13);
        b.call_num(12);
        assert!(b.check_cols())
    }

    #[test]
    fn test_bingo_board_play() {
        let mut b = Board::new(&TEST_BOARD);
        b.play(&TEST_PLAY_CALLS);
        assert_eq!(b.win_call.unwrap(), 24);
        assert_eq!(b.win_rank.unwrap(), 11);
    }

    #[test]
    fn test_bingo_board_score() {
        let mut b = Board::new(&TEST_BOARD);
        b.play(&TEST_PLAY_CALLS);
        assert_eq!(b.score().unwrap(), 4512);
    }
}
