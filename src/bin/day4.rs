//! --- Day 5: Hydrothermal Venture ---
//!
//! You come across a field of hydrothermal vents on the ocean floor! These vents constantly produce large, opaque clouds, so it would be best to avoid them if possible.
//!
//! They tend to form in lines; the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for you to review. For example:
//!
//! 0,9 -> 5,9
//! 8,0 -> 0,8
//! 9,4 -> 3,4
//! 2,2 -> 2,1
//! 7,0 -> 7,4
//! 6,4 -> 2,0
//! 0,9 -> 2,9
//! 3,4 -> 1,4
//! 0,0 -> 8,8
//! 5,5 -> 8,2
//!
//! Each line of vents is given as a line segment in the format x1,y1 -> x2,y2 where x1,y1 are the coordinates of one end the line segment and x2,y2 are the coordinates of the other end. These line segments include the points at both ends. In other words:
//!
//!     An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
//!     An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.
//!
//! For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.
//!
//! So, the horizontal and vertical lines from the above list would produce the following diagram:
//!
//! .......1..
//! ..1....1..
//! ..1....1..
//! .......1..
//! .112111211
//! ..........
//! ..........
//! ..........
//! ..........
//! 222111....
//!
//! In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9. Each position is shown as the number of lines which cover that point or . if no line covers that point. The top-left pair of 1s, for example, comes from 2,2 -> 2,1; the very bottom row is formed by the overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.
//!
//! To avoid the most dangerous areas, you need to determine the number of points where at least two lines overlap. In the above example, this is anywhere in the diagram with a 2 or larger - a total of 5 points.
//!
//! Consider only horizontal and vertical lines. At how many points do at least two lines overlap?
//!
//!--- Part Two ---
//!
//! Unfortunately, considering only horizontal and vertical lines doesn't give you the full picture; you need to also consider diagonal lines.
//!
//! Because of the limits of the hydrothermal vent mapping system, the lines in your list will only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees. In other words:
//!
//!     An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
//!     An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
//!
//! Considering all lines from the above example would now produce the following diagram:
//!
//! 1.1....11.
//! .111...2..
//! ..2.1.111.
//! ...1.2.2..
//! .112313211
//! ...1.2....
//! ..1...1...
//! .1.....1..
//! 1.......1.
//! 222111....
//!
//! You still need to determine the number of points where at least two lines overlap. In the above example, this is still anywhere in the diagram with a 2 or larger - now a total of 12 points.
//!
//! Consider all of the lines. At how many points do at least two lines overlap?
//!

use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let file = input_file("input/day4.input").unwrap();
    let mut lines = parse_lines(&file[..]).unwrap();

    let mut v = Vents::new(false);
    for l in [] {
        lines.push(Line::new(&l))
    }
    v.add_lines(&lines);
    println!("Dangerous Vents: {}", v.get_danger());

    let mut v = Vents::new(true);
    for l in [] {
        lines.push(Line::new(&l))
    }
    v.add_lines(&lines);
    println!("Dangerous Vents with Diagonal: {}", v.get_danger());
}

fn input_file(filename: &str) -> Result<Vec<String>, Box<dyn Error>> {
    let path = Path::new(filename);
    let file = File::open(&path)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

fn parse_lines(input: &[String]) -> Result<Vec<Line>, Box<dyn Error>> {
    let mut lines = Vec::new();

    for line in input.iter() {
        let mut numbers = Vec::<i32>::new();
        let mut points = line.split_whitespace().collect::<Vec<&str>>();
        points.remove(1);
        for p in points.iter() {
            for n in p.split(",") {
                numbers.push(n.parse::<i32>().unwrap());
            }
        }

        lines.push(Line::new(&numbers[..].try_into()?));
    }

    Ok(lines)
}

#[derive(Debug)]
struct Vents {
    diag: bool,
    map: HashMap<Point, i32>,
}

impl Vents {
    pub fn new(diag: bool) -> Vents {
        Vents {
            diag: diag,
            map: HashMap::new(),
        }
    }

    fn add_line(&mut self, line: &Line) {
        if let Some(points) = line.get_points(self.diag) {
            for p in points {
                let count = self.map.entry(p).or_insert(0);
                *count += 1;
            }
        }
    }

    pub fn add_lines(&mut self, lines: &[Line]) {
        for l in lines {
            self.add_line(l)
        }
    }

    pub fn get_danger(&self) -> i32 {
        self.map
            .values()
            .filter(|&x| *x > 1)
            .collect::<Vec<&i32>>()
            .len() as i32
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct Line {
    start: Point,
    finish: Point,
}

impl Line {
    pub fn new(coords: &[i32; 4]) -> Line {
        Line {
            start: Point {
                x: coords[0],
                y: coords[1],
            },
            finish: Point {
                x: coords[2],
                y: coords[3],
            },
        }
    }

    fn get_vert_points(&self) -> Option<Vec<Point>> {
        if self.start.y == self.finish.y {
            let mut p = Vec::new();
            let range = if self.start.x <= self.finish.x {
                self.start.x..self.finish.x + 1
            } else {
                self.finish.x..self.start.x + 1
            };
            for i in range {
                p.push(Point {
                    x: i,
                    y: self.start.y,
                });
            }
            Some(p)
        } else {
            None
        }
    }

    fn get_horiz_points(&self) -> Option<Vec<Point>> {
        if self.start.x == self.finish.x {
            let mut p = Vec::new();
            let range = if self.start.y <= self.finish.y {
                self.start.y..self.finish.y + 1
            } else {
                self.finish.y..self.start.y + 1
            };
            for i in range {
                p.push(Point {
                    x: self.start.x,
                    y: i,
                });
            }
            Some(p)
        } else {
            None
        }
    }

    fn get_diag_points(&self) -> Option<Vec<Point>> {
        if (self.finish.x - self.start.x).abs() == (self.finish.y - self.start.y).abs() {
            let mut p = Vec::new();

            let x_add = if self.finish.x >= self.start.x { 1 } else { -1 };
            let y_add = if self.finish.y >= self.start.y { 1 } else { -1 };

            for i in 0..=(self.finish.x - self.start.x).abs() {
                p.push(Point {
                    x: self.start.x + x_add * i,
                    y: self.start.y + y_add * i,
                });
            }

            Some(p)
        } else {
            None
        }
    }

    pub fn get_points(&self, diag: bool) -> Option<Vec<Point>> {
        match self.get_vert_points() {
            Some(p) => Some(p),
            None => match self.get_horiz_points() {
                Some(p) => Some(p),
                None => {
                    if diag {
                        match self.get_diag_points() {
                            Some(p) => Some(p),
                            None => None,
                        }
                    } else {
                        None
                    }
                }
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_LINES: [[i32; 4]; 10] = [
        [0, 9, 5, 9],
        [8, 0, 0, 8],
        [9, 4, 3, 4],
        [2, 2, 2, 1],
        [7, 0, 7, 4],
        [6, 4, 2, 0],
        [0, 9, 2, 9],
        [3, 4, 1, 4],
        [0, 0, 8, 8],
        [5, 5, 8, 2],
    ];

    #[test]
    fn test_vents_new() {
        let v = Vents::new(false);
        println!("Vents: {:?}", v);
    }

    #[test]
    fn test_vents_add_line() {
        let mut v = Vents::new(false);
        let l = Line::new(&TEST_LINES[0]);
        v.add_line(&l);
        assert_eq!(*v.map.get(&Point { x: 3, y: 9 }).unwrap(), 1);
        assert_eq!(v.map.get(&Point { x: 3, y: 8 }), None);
    }

    #[test]
    fn test_vents_add_lines() {
        let mut v = Vents::new(false);
        let mut lines = Vec::new();
        for l in TEST_LINES {
            lines.push(Line::new(&l))
        }
        v.add_lines(&lines);
        assert_eq!(*v.map.get(&Point { x: 3, y: 9 }).unwrap(), 1);
        assert_eq!(v.map.get(&Point { x: 3, y: 8 }), None);
    }

    #[test]
    fn test_vents_get_danger() {
        let mut v = Vents::new(false);
        let mut lines = Vec::new();
        for l in TEST_LINES {
            lines.push(Line::new(&l))
        }
        v.add_lines(&lines);
        assert_eq!(v.get_danger(), 5);
    }

    #[test]
    fn test_vents_get_danger_diagnol() {
        let mut v = Vents::new(true);
        let mut lines = Vec::new();
        for l in TEST_LINES {
            lines.push(Line::new(&l))
        }
        v.add_lines(&lines);
        assert_eq!(v.get_danger(), 12);
    }

    #[test]
    fn test_lines_new() {
        let l = Line::new(&TEST_LINES[0]);
        println!("Line: {:?}", l);
    }

    #[test]
    fn test_lines_get_vert_points() {
        let l = Line::new(&[0, 3, 3, 3]);
        assert_eq!(
            l.get_vert_points().unwrap(),
            vec![
                Point { x: 0, y: 3 },
                Point { x: 1, y: 3 },
                Point { x: 2, y: 3 },
                Point { x: 3, y: 3 },
            ]
        );
    }

    #[test]
    fn test_lines_get_vert_points_reverse() {
        let l = Line::new(&[9, 4, 3, 4]);
        assert_eq!(
            l.get_vert_points().unwrap(),
            vec![
                Point { x: 3, y: 4 },
                Point { x: 4, y: 4 },
                Point { x: 5, y: 4 },
                Point { x: 6, y: 4 },
                Point { x: 7, y: 4 },
                Point { x: 8, y: 4 },
                Point { x: 9, y: 4 },
            ]
        );
    }

    #[test]
    fn test_lines_get_vert_points_none() {
        let l = Line::new(&[0, 3, 3, 4]);
        assert_eq!(l.get_vert_points(), None);
    }

    #[test]
    fn test_lines_get_horiz_points() {
        let l = Line::new(&[3, 0, 3, 3]);
        assert_eq!(
            l.get_horiz_points().unwrap(),
            vec![
                Point { x: 3, y: 0 },
                Point { x: 3, y: 1 },
                Point { x: 3, y: 2 },
                Point { x: 3, y: 3 },
            ]
        );
    }

    #[test]
    fn test_lines_get_horiz_points_none() {
        let l = Line::new(&[0, 3, 3, 4]);
        assert_eq!(l.get_horiz_points(), None);
    }

    #[test]
    fn test_lines_get_diag_points() {
        let l = Line::new(&[1, 1, 3, 3]);
        assert_eq!(
            l.get_diag_points().unwrap(),
            vec![
                Point { x: 1, y: 1 },
                Point { x: 2, y: 2 },
                Point { x: 3, y: 3 },
            ]
        );
    }

    #[test]
    fn test_lines_get_diag_points_reverse() {
        let l = Line::new(&[9, 7, 7, 9]);
        assert_eq!(
            l.get_diag_points().unwrap(),
            vec![
                Point { x: 9, y: 7 },
                Point { x: 8, y: 8 },
                Point { x: 7, y: 9 },
            ]
        );
    }

    #[test]
    fn test_lines_get_diag_points_none() {
        let l = Line::new(&[0, 3, 3, 4]);
        assert_eq!(l.get_diag_points(), None);
    }

    #[test]
    fn test_lines_get_points() {
        let l = Line::new(&[3, 0, 3, 3]);
        assert_eq!(
            l.get_points(false).unwrap(),
            vec![
                Point { x: 3, y: 0 },
                Point { x: 3, y: 1 },
                Point { x: 3, y: 2 },
                Point { x: 3, y: 3 },
            ]
        );
    }

    #[test]
    fn test_lines_get_points_none() {
        let l = Line::new(&[0, 3, 3, 4]);
        assert_eq!(l.get_points(false), None);
    }
}
