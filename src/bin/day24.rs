//! --- Day 25: Sea Cucumber ---
//!
//! This is it: the bottom of the ocean trench, the last place the sleigh keys could be. Your submarine's experimental antenna still isn't boosted enough to detect the keys, but they must be here. All you need to do is reach the seafloor and find them.
//!
//! At least, you'd touch down on the seafloor if you could; unfortunately, it's completely covered by two large herds of sea cucumbers, and there isn't an open space large enough for your submarine.
//!
//! You suspect that the Elves must have done this before, because just then you discover the phone number of a deep-sea marine biologist on a handwritten note taped to the wall of the submarine's cockpit.
//!
//! "Sea cucumbers? Yeah, they're probably hunting for food. But don't worry, they're predictable critters: they move in perfectly straight lines, only moving forward when there's space to do so. They're actually quite polite!"
//!
//! You explain that you'd like to predict when you could land your submarine.
//!
//! "Oh that's easy, they'll eventually pile up and leave enough space for-- wait, did you say submarine? And the only place with that many sea cucumbers would be at the very bottom of the Mariana--" You hang up the phone.
//!
//! There are two herds of sea cucumbers sharing the same region; one always moves east (>), while the other always moves south (v). Each location can contain at most one sea cucumber; the remaining locations are empty (.). The submarine helpfully generates a map of the situation (your puzzle input). For example:
//!
//! v...>>.vv>
//! .vv>>.vv..
//! >>.>v>...v
//! >>v>>.>.v.
//! v>v.vv.v..
//! >.>>..v...
//! .vv..>.>v.
//! v.v..>>v.v
//! ....v..v.>
//!
//! Every step, the sea cucumbers in the east-facing herd attempt to move forward one location, then the sea cucumbers in the south-facing herd attempt to move forward one location. When a herd moves forward, every sea cucumber in the herd first simultaneously considers whether there is a sea cucumber in the adjacent location it's facing (even another sea cucumber facing the same direction), and then every sea cucumber facing an empty location simultaneously moves into that location.
//!
//! So, in a situation like this:
//!
//! ...>>>>>...
//!
//! After one step, only the rightmost sea cucumber would have moved:
//!
//! ...>>>>.>..
//!
//! After the next step, two sea cucumbers move:
//!
//! ...>>>.>.>.
//!
//! During a single step, the east-facing herd moves first, then the south-facing herd moves. So, given this situation:
//!
//! ..........
//! .>v....v..
//! .......>..
//! ..........
//!
//! After a single step, of the sea cucumbers on the left, only the south-facing sea cucumber has moved (as it wasn't out of the way in time for the east-facing cucumber on the left to move), but both sea cucumbers on the right have moved (as the east-facing sea cucumber moved out of the way of the south-facing sea cucumber):
//!
//! ..........
//! .>........
//! ..v....v>.
//! ..........
//!
//! Due to strong water currents in the area, sea cucumbers that move off the right edge of the map appear on the left edge, and sea cucumbers that move off the bottom edge of the map appear on the top edge. Sea cucumbers always check whether their destination location is empty before moving, even if that destination is on the opposite side of the map:
//!
//! Initial state:
//! ...>...
//! .......
//! ......>
//! v.....>
//! ......>
//! .......
//! ..vvv..
//!
//! After 1 step:
//! ..vv>..
//! .......
//! >......
//! v.....>
//! >......
//! .......
//! ....v..
//!
//! After 2 steps:
//! ....v>.
//! ..vv...
//! .>.....
//! ......>
//! v>.....
//! .......
//! .......
//!
//! After 3 steps:
//! ......>
//! ..v.v..
//! ..>v...
//! >......
//! ..>....
//! v......
//! .......
//!
//! After 4 steps:
//! >......
//! ..v....
//! ..>.v..
//! .>.v...
//! ...>...
//! .......
//! v......
//!
//! To find a safe place to land your submarine, the sea cucumbers need to stop moving. Again consider the first example:
//!
//! Initial state:
//! v...>>.vv>
//! .vv>>.vv..
//! >>.>v>...v
//! >>v>>.>.v.
//! v>v.vv.v..
//! >.>>..v...
//! .vv..>.>v.
//! v.v..>>v.v
//! ....v..v.>
//!
//! After 1 step:
//! ....>.>v.>
//! v.v>.>v.v.
//! >v>>..>v..
//! >>v>v>.>.v
//! .>v.v...v.
//! v>>.>vvv..
//! ..v...>>..
//! vv...>>vv.
//! >.v.v..v.v
//!
//! After 2 steps:
//! >.v.v>>..v
//! v.v.>>vv..
//! >v>.>.>.v.
//! >>v>v.>v>.
//! .>..v....v
//! .>v>>.v.v.
//! v....v>v>.
//! .vv..>>v..
//! v>.....vv.
//!
//! After 3 steps:
//! v>v.v>.>v.
//! v...>>.v.v
//! >vv>.>v>..
//! >>v>v.>.v>
//! ..>....v..
//! .>.>v>v..v
//! ..v..v>vv>
//! v.v..>>v..
//! .v>....v..
//!
//! After 4 steps:
//! v>..v.>>..
//! v.v.>.>.v.
//! >vv.>>.v>v
//! >>.>..v>.>
//! ..v>v...v.
//! ..>>.>vv..
//! >.v.vv>v.v
//! .....>>vv.
//! vvv>...v..
//!
//! After 5 steps:
//! vv>...>v>.
//! v.v.v>.>v.
//! >.v.>.>.>v
//! >v>.>..v>>
//! ..v>v.v...
//! ..>.>>vvv.
//! .>...v>v..
//! ..v.v>>v.v
//! v.v.>...v.
//!
//! ...
//!
//! After 10 steps:
//! ..>..>>vv.
//! v.....>>.v
//! ..v.v>>>v>
//! v>.>v.>>>.
//! ..v>v.vv.v
//! .v.>>>.v..
//! v.v..>v>..
//! ..v...>v.>
//! .vv..v>vv.
//!
//! ...
//!
//! After 20 steps:
//! v>.....>>.
//! >vv>.....v
//! .>v>v.vv>>
//! v>>>v.>v.>
//! ....vv>v..
//! .v.>>>vvv.
//! ..v..>>vv.
//! v.v...>>.v
//! ..v.....v>
//!
//! ...
//!
//! After 30 steps:
//! .vv.v..>>>
//! v>...v...>
//! >.v>.>vv.>
//! >v>.>.>v.>
//! .>..v.vv..
//! ..v>..>>v.
//! ....v>..>v
//! v.v...>vv>
//! v.v...>vvv
//!
//! ...
//!
//! After 40 steps:
//! >>v>v..v..
//! ..>>v..vv.
//! ..>>>v.>.v
//! ..>>>>vvv>
//! v.....>...
//! v.v...>v>>
//! >vv.....v>
//! .>v...v.>v
//! vvv.v..v.>
//!
//! ...
//!
//! After 50 steps:
//! ..>>v>vv.v
//! ..v.>>vv..
//! v.>>v>>v..
//! ..>>>>>vv.
//! vvv....>vv
//! ..v....>>>
//! v>.......>
//! .vv>....v>
//! .>v.vv.v..
//!
//! ...
//!
//! After 55 steps:
//! ..>>v>vv..
//! ..v.>>vv..
//! ..>>v>>vv.
//! ..>>>>>vv.
//! v......>vv
//! v>v....>>v
//! vvv...>..>
//! >vv.....>.
//! .>v.vv.v..
//!
//! After 56 steps:
//! ..>>v>vv..
//! ..v.>>vv..
//! ..>>v>>vv.
//! ..>>>>>vv.
//! v......>vv
//! v>v....>>v
//! vvv....>.>
//! >vv......>
//! .>v.vv.v..
//!
//! After 57 steps:
//! ..>>v>vv..
//! ..v.>>vv..
//! ..>>v>>vv.
//! ..>>>>>vv.
//! v......>vv
//! v>v....>>v
//! vvv.....>>
//! >vv......>
//! .>v.vv.v..
//!
//! After 58 steps:
//! ..>>v>vv..
//! ..v.>>vv..
//! ..>>v>>vv.
//! ..>>>>>vv.
//! v......>vv
//! v>v....>>v
//! vvv.....>>
//! >vv......>
//! .>v.vv.v..
//!
//! In this example, the sea cucumbers stop moving after 58 steps.
//!
//! Find somewhere safe to land your submarine. What is the first step on which no sea cucumbers move?
//!

use std::collections::HashMap;
use std::fmt;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day24.input").unwrap();
    let mut h = Herd::from_lines(lines);
    println!("Land after {} steps.", h.find());
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Dir {
    East,
    South,
}

impl Dir {
    pub fn from_char(a: char) -> Option<Dir> {
        if a == '>' {
            Some(Dir::East)
        } else if a == 'v' {
            Some(Dir::South)
        } else {
            None
        }
    }
}

#[derive(Debug, PartialEq)]
struct Herd {
    map: HashMap<(usize, usize), Dir>,
    max: (usize, usize),
}

impl fmt::Display for Herd {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut out = String::new();

        let mut v = Vec::new();
        let mut t = Vec::new();
        t.resize((self.max.0 + 1).try_into().unwrap(), '.');
        for _i in 0..self.max.1 + 1 {
            v.push(t.clone());
        }

        for ((x, y), d) in self.map.iter() {
            match d {
                Dir::East => v[*y][*x] = '>',
                Dir::South => v[*y][*x] = 'v',
            }
        }

        for i in v.iter() {
            let s: String = i.into_iter().collect();
            out.push_str(&s);
            out.push('\n');
        }
        write!(f, "\n{}", out)
    }
}

impl Herd {
    pub fn from_lines(lines: Vec<String>) -> Herd {
        let mut h = Herd {
            map: HashMap::new(),
            max: (0, 0),
        };

        for (y, l) in lines.iter().enumerate() {
            if y > h.max.1 {
                h.max.1 = y;
            }
            for (x, c) in l.chars().enumerate() {
                if x > h.max.0 {
                    h.max.0 = x;
                }
                if let Some(d) = Dir::from_char(c) {
                    h.map.insert((x, y), d);
                }
            }
        }

        h
    }

    pub fn find(&mut self) -> usize {
        let mut i = 1;
        loop {
            if !self.step() {
                return i;
            } else {
                i += 1;
            }
        }
    }

    pub fn step(&mut self) -> bool {
        let mut moved = false;
        moved |= self.step_dir(Dir::East);
        moved |= self.step_dir(Dir::South);
        moved
    }

    fn step_dir(&mut self, filter: Dir) -> bool {
        let mut moved = false;
        let fresh = self.map.clone();

        self.map.clear();

        for ((x, y), d) in fresh.iter() {
            if *d == filter {
                let mut new_p = (*x, *y);
                match filter {
                    Dir::East => {
                        if new_p.0 < self.max.0 {
                            new_p.0 += 1;
                        } else {
                            new_p.0 = 0;
                        }
                    }
                    Dir::South => {
                        if new_p.1 < self.max.1 {
                            new_p.1 += 1;
                        } else {
                            new_p.1 = 0;
                        }
                    }
                }
                if !fresh.contains_key(&new_p) {
                    moved = true;
                    self.map.insert(new_p, *d);
                } else {
                    self.map.insert((*x, *y), *d);
                }
            } else {
                self.map.insert((*x, *y), *d);
            }
        }

        moved
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dir_from_char() {
        assert_eq!(Dir::from_char('v'), Some(Dir::South));
        assert_eq!(Dir::from_char('>'), Some(Dir::East));
        assert_eq!(Dir::from_char('.'), None);
    }

    #[test]
    fn test_herd_from_lines() {
        let l = vec![
            "v...>>.vv>".to_string(),
            ".vv>>.vv..".to_string(),
            ">>.>v>...v".to_string(),
            ">>v>>.>.v.".to_string(),
            "v>v.vv.v..".to_string(),
            ">.>>..v...".to_string(),
            ".vv..>.>v.".to_string(),
            "v.v..>>v.v".to_string(),
            "....v..v.>".to_string(),
        ];
        let h = Herd::from_lines(l);
        assert_eq!(h.max, (9, 8));
        assert_eq!(h.map.get(&(0, 0)), Some(&Dir::South));
        assert_eq!(h.map.get(&(3, 4)), None);
        assert_eq!(h.map.get(&(9, 8)), Some(&Dir::East));
    }

    #[test]
    fn test_herd_step_dir_1() {
        let l = vec!["...>>>>>...".to_string()];
        let mut h = Herd::from_lines(l);
        h.step_dir(Dir::East);
        assert_eq!(h.map.get(&(7, 0)), None);
        assert_eq!(h.map.get(&(8, 0)), Some(&Dir::East));
        assert_eq!(h.map.get(&(9, 0)), None);
    }

    #[test]
    fn test_herd_step_dir_2() {
        let l = vec!["...>>>>>...".to_string()];
        let mut h = Herd::from_lines(l);
        h.step_dir(Dir::East);
        println!("{}", h);
        h.step_dir(Dir::East);
        println!("{}", h);
        h.step_dir(Dir::East);
        println!("{}", h);
        h.step_dir(Dir::East);
        println!("{}", h);
        assert_eq!(h.map.get(&(8, 0)), None);
        assert_eq!(h.map.get(&(0, 0)), Some(&Dir::East));
        assert_eq!(h.map.get(&(1, 0)), None);
    }

    #[test]
    fn test_herd_step() {
        let l = vec![
            "..........".to_string(),
            ".>v....v..".to_string(),
            ".......>..".to_string(),
            "..........".to_string(),
        ];
        let mut h1 = Herd::from_lines(l);
        println!("{}", h1);
        h1.step();
        let l = vec![
            "..........".to_string(),
            ".>........".to_string(),
            "..v....v>.".to_string(),
            "..........".to_string(),
        ];
        let h2 = Herd::from_lines(l);
        println!("{}", h1);
        println!("{}", h2);
        assert_eq!(h1, h2);
    }

    #[test]
    fn test_herd_find() {
        let l = vec![
            "v...>>.vv>".to_string(),
            ".vv>>.vv..".to_string(),
            ">>.>v>...v".to_string(),
            ">>v>>.>.v.".to_string(),
            "v>v.vv.v..".to_string(),
            ">.>>..v...".to_string(),
            ".vv..>.>v.".to_string(),
            "v.v..>>v.v".to_string(),
            "....v..v.>".to_string(),
        ];
        let mut h = Herd::from_lines(l);
        assert_eq!(h.find(), 58);
    }
}
