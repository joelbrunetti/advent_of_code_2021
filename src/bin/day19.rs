//! --- Day 20: Trench Map ---
//!
//! With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.
//!
//! When you get back the image from the scanners, it seems to just be random noise. Perhaps you can combine an image enhancement algorithm and the input image (your puzzle input) to clean it up a little.
//!
//! For example:
//!
//! ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
//! #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
//! .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
//! .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
//! .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
//! ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
//! ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#
//!
//! #..#.
//! #....
//! ##..#
//! ..#..
//! ..###
//!
//! The first section is the image enhancement algorithm. It is normally given on a single line, but it has been wrapped to multiple lines in this example for legibility. The second section is the input image, a two-dimensional grid of light pixels (#) and dark pixels (.).
//!
//! The image enhancement algorithm describes how to enhance an image by simultaneously converting all pixels in the input image into an output image. Each pixel of the output image is determined by looking at a 3x3 square of pixels centered on the corresponding input image pixel. So, to determine the value of the pixel at (5,10) in the output image, nine pixels from the input image need to be considered: (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9), (6,10), and (6,11). These nine input pixels are combined into a single binary number that is used as an index in the image enhancement algorithm string.
//!
//! For example, to determine the output pixel that corresponds to the very middle pixel of the input image, the nine pixels marked by [...] would need to be considered:
//!
//! # . . # .
//! #[. . .].
//! #[# . .]#
//! .[. # .].
//! . . # # #
//!
//! Starting from the top-left and reading across each row, these pixels are ..., then #.., then .#.; combining these forms ...#...#.. By turning dark pixels (.) into 0 and light pixels (#) into 1, the binary number 000100010 can be formed, which is 34 in decimal.
//!
//! The image enhancement algorithm string is exactly 512 characters long, enough to match every possible 9-bit binary number. The first few characters of the string (numbered starting from zero) are as follows:
//!
//! 0         10        20        30  34    40        50        60        70
//! |         |         |         |   |     |         |         |         |
//! ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
//!
//! In the middle of this first group of characters, the character at index 34 can be found: #. So, the output pixel in the center of the output image should be #, a light pixel.
//!
//! This process can then be repeated to calculate every pixel of the output image.
//!
//! Through advances in imaging technology, the images being operated on here are infinite in size. Every pixel of the infinite output image needs to be calculated exactly based on the relevant pixels of the input image. The small input image you have is only a small region of the actual infinite input image; the rest of the input image consists of dark pixels (.). For the purposes of the example, to save on space, only a portion of the infinite-sized input and output images will be shown.
//!
//! The starting input image, therefore, looks something like this, with more dark pixels (.) extending forever in every direction not shown here:
//!
//! ...............
//! ...............
//! ...............
//! ...............
//! ...............
//! .....#..#......
//! .....#.........
//! .....##..#.....
//! .......#.......
//! .......###.....
//! ...............
//! ...............
//! ...............
//! ...............
//! ...............
//!
//! By applying the image enhancement algorithm to every pixel simultaneously, the following output image can be obtained:
//!
//! ...............
//! ...............
//! ...............
//! ...............
//! .....##.##.....
//! ....#..#.#.....
//! ....##.#..#....
//! ....####..#....
//! .....#..##.....
//! ......##..#....
//! .......#.#.....
//! ...............
//! ...............
//! ...............
//! ...............
//!
//! Through further advances in imaging technology, the above output image can also be used as an input image! This allows it to be enhanced a second time:
//!
//! ...............
//! ...............
//! ...............
//! ..........#....
//! ....#..#.#.....
//! ...#.#...###...
//! ...#...##.#....
//! ...#.....#.#...
//! ....#.#####....
//! .....#.#####...
//! ......##.##....
//! .......###.....
//! ...............
//! ...............
//! ...............
//!
//! Truly incredible - now the small details are really starting to come through. After enhancing the original input image twice, 35 pixels are lit.
//!
//! Start with the original input image and apply the image enhancement algorithm twice, being careful to account for the infinite size of the images. How many pixels are lit in the resulting image?
//!
//!--- Part Two ---
//!
//! You still can't quite make out the details in the image. Maybe you just didn't enhance it enough.
//!
//! If you enhance the starting input image in the above example a total of 50 times, 3351 pixels are lit in the final output image.
//!
//! Start again with the original input image and apply the image enhancement algorithm 50 times. How many pixels are lit in the resulting image?
//!

use std::collections::HashMap;

fn main() {
    let lines = advent_of_code_2021::input_file("input/day19.input").unwrap();
    let mut i = Image::from_vec_string(&lines);
    i.enhance_repeat(2);
    println!("Pixel Count after 2 rounds: {}", i.count_pixels());
    i.enhance_repeat(48);
    println!("Pixel Count after 50 rounds: {}", i.count_pixels());
}

struct Image {
    enhance: Vec<bool>,
    buf: [HashMap<(i32, i32), bool>; 2],
    index: usize,
    outside: bool,
    x_limits: (i32, i32),
    y_limits: (i32, i32),
}

impl Image {
    pub fn from_vec_string(lines: &Vec<String>) -> Image {
        let mut image = Image {
            enhance: Vec::new(),
            buf: [HashMap::new(), HashMap::new()],
            index: 0,
            outside: false,
            x_limits: (0, 0),
            y_limits: (0, 0),
        };

        let mut lines_iter = lines.iter();

        for c in lines_iter.next().unwrap().chars() {
            if c == '.' {
                image.enhance.push(false);
            } else if c == '#' {
                image.enhance.push(true);
            } else {
                panic!("Invalid enhance input.");
            }
        }

        lines_iter.next();

        for (y, l) in lines_iter.enumerate() {
            if y as i32 > image.y_limits.1 {
                image.y_limits.1 = y as i32;
            }

            for (x, c) in l.chars().enumerate() {
                if x as i32 > image.x_limits.1 {
                    image.x_limits.1 = x as i32;
                }

                if c == '#' {
                    image.buf[image.index].insert((x as i32, y as i32), true);
                } else if c == '.' {
                    image.buf[image.index].insert((x as i32, y as i32), false);
                } else {
                    panic!("Invalid image input.");
                }
            }
        }

        image
    }

    pub fn enhance_repeat(&mut self, count: u32) {
        for _i in 0..count {
            self.enhance();
        }
    }

    pub fn enhance(&mut self) {
        let next_index = if self.index == 0 { 1 } else { 0 };
        self.buf[next_index].clear();
        self.x_limits = (self.x_limits.0 - 1, self.x_limits.1 + 1);
        self.y_limits = (self.y_limits.0 - 1, self.y_limits.1 + 1);

        for y in self.y_limits.0..=self.y_limits.1 {
            for x in self.x_limits.0..=self.x_limits.1 {
                self.buf[next_index].insert((x, y), self.find_enhanced_pixel((x, y)));
            }
        }

        if self.outside {
            self.outside = *self.enhance.last().unwrap();
        } else {
            self.outside = self.enhance[0];
        }

        self.index = next_index;
    }

    fn find_enhanced_pixel(&self, loc: (i32, i32)) -> bool {
        let mut index: usize = 0;
        for y in loc.1 - 1..=loc.1 + 1 {
            for x in loc.0 - 1..=loc.0 + 1 {
                index = index << 1;
                match self.buf[self.index].get(&(x, y)) {
                    Some(p) => {
                        if *p {
                            index += 1
                        }
                    }
                    None => {
                        if self.outside {
                            index += 1
                        }
                    }
                }
            }
        }
        self.enhance[index]
    }

    pub fn count_pixels(&self) -> usize {
        let mut count = 0;
        for p in self.buf[self.index].values() {
            if *p {
                count += 1;
            }
        }
        count
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_image_from_vec_string() {
        let test = vec![
            "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#".to_string(),
            "".to_string(),
            "#..#.".to_string(),
            "#....".to_string(),
            "##..#".to_string(),
            "..#..".to_string(),
            "..###".to_string(),
        ];
        let i = Image::from_vec_string(&test);
        assert_eq!(i.x_limits, (0, 4));
        assert_eq!(i.y_limits, (0, 4));
        assert_eq!(i.count_pixels(), 10);
    }

    #[test]
    fn test_image_enhance() {
        let test = vec![
            "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#".to_string(),
            "".to_string(),
            "#..#.".to_string(),
            "#....".to_string(),
            "##..#".to_string(),
            "..#..".to_string(),
            "..###".to_string(),
        ];
        let mut i = Image::from_vec_string(&test);
        assert_eq!(i.count_pixels(), 10);
        i.enhance();
        assert_eq!(i.count_pixels(), 24);
        i.enhance();
        assert_eq!(i.count_pixels(), 35);
    }

    #[test]
    fn test_image_enhance_repeat() {
        let test = vec![
            "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#".to_string(),
            "".to_string(),
            "#..#.".to_string(),
            "#....".to_string(),
            "##..#".to_string(),
            "..#..".to_string(),
            "..###".to_string(),
        ];
        let mut i = Image::from_vec_string(&test);
        assert_eq!(i.count_pixels(), 10);
        i.enhance_repeat(2);
        assert_eq!(i.count_pixels(), 35);
        i.enhance_repeat(48);
        assert_eq!(i.count_pixels(), 3351);
    }
}
