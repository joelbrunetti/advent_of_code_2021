//! --- Day 15: Chiton ---
//!
//! You've almost reached the exit of the cave, but the walls are getting closer together. Your submarine can barely still fit, though; the main problem is that the walls of the cave are covered in chitons, and it would be best not to bump any of them.
//!
//! The cavern is large, but has a very low ceiling, restricting your motion to two dimensions. The shape of the cavern resembles a square; a quick scan of chiton density produces a map of risk level throughout the cave (your puzzle input). For example:
//!
//! 1163751742
//! 1381373672
//! 2136511328
//! 3694931569
//! 7463417111
//! 1319128137
//! 1359912421
//! 3125421639
//! 1293138521
//! 2311944581
//!
//! You start in the top left position, your destination is the bottom right position, and you cannot move diagonally. The number at each position is its risk level; to determine the total risk of an entire path, add up the risk levels of each position you enter (that is, don't count the risk level of your starting position unless you enter it; leaving it adds no risk to your total).
//!
//! Your goal is to find a path with the lowest total risk. In this example, a path with the lowest total risk is highlighted here:
//!
//! 1163751742
//! 1381373672
//! 2136511328
//! 3694931569
//! 7463417111
//! 1319128137
//! 1359912421
//! 3125421639
//! 1293138521
//! 2311944581
//!
//! The total risk of this path is 40 (the starting position is never entered, so its risk is not counted).
//!
//! What is the lowest total risk of any path from the top left to the bottom right?
//!
//!--- Part Two ---
//!
//! Now that you know how to find low-risk paths in the cave, you can try to find your way out.
//!
//! The entire cave is actually five times larger in both dimensions than you thought; the area you originally scanned is just one tile in a 5x5 tile area that forms the full map. Your original map tile repeats to the right and downward; each time the tile repeats to the right or downward, all of its risk levels are 1 higher than the tile immediately up or left of it. However, risk levels above 9 wrap back around to 1. So, if your original map had some position with a risk level of 8, then that same position on each of the 25 total tiles would be as follows:
//!
//! 8 9 1 2 3
//! 9 1 2 3 4
//! 1 2 3 4 5
//! 2 3 4 5 6
//! 3 4 5 6 7
//!
//! Each single digit above corresponds to the example position with a value of 8 on the top-left tile. Because the full map is actually five times larger in both dimensions, that position appears a total of 25 times, once in each duplicated tile, with the values shown above.
//!
//! Here is the full five-times-as-large version of the first example above, with the original map in the top left corner highlighted:
//!
//! 11637517422274862853338597396444961841755517295286
//! 13813736722492484783351359589446246169155735727126
//! 21365113283247622439435873354154698446526571955763
//! 36949315694715142671582625378269373648937148475914
//! 74634171118574528222968563933317967414442817852555
//! 13191281372421239248353234135946434524615754563572
//! 13599124212461123532357223464346833457545794456865
//! 31254216394236532741534764385264587549637569865174
//! 12931385212314249632342535174345364628545647573965
//! 23119445813422155692453326671356443778246755488935
//! 22748628533385973964449618417555172952866628316397
//! 24924847833513595894462461691557357271266846838237
//! 32476224394358733541546984465265719557637682166874
//! 47151426715826253782693736489371484759148259586125
//! 85745282229685639333179674144428178525553928963666
//! 24212392483532341359464345246157545635726865674683
//! 24611235323572234643468334575457944568656815567976
//! 42365327415347643852645875496375698651748671976285
//! 23142496323425351743453646285456475739656758684176
//! 34221556924533266713564437782467554889357866599146
//! 33859739644496184175551729528666283163977739427418
//! 35135958944624616915573572712668468382377957949348
//! 43587335415469844652657195576376821668748793277985
//! 58262537826937364893714847591482595861259361697236
//! 96856393331796741444281785255539289636664139174777
//! 35323413594643452461575456357268656746837976785794
//! 35722346434683345754579445686568155679767926678187
//! 53476438526458754963756986517486719762859782187396
//! 34253517434536462854564757396567586841767869795287
//! 45332667135644377824675548893578665991468977611257
//! 44961841755517295286662831639777394274188841538529
//! 46246169155735727126684683823779579493488168151459
//! 54698446526571955763768216687487932779859814388196
//! 69373648937148475914825958612593616972361472718347
//! 17967414442817852555392896366641391747775241285888
//! 46434524615754563572686567468379767857948187896815
//! 46833457545794456865681556797679266781878137789298
//! 64587549637569865174867197628597821873961893298417
//! 45364628545647573965675868417678697952878971816398
//! 56443778246755488935786659914689776112579188722368
//! 55172952866628316397773942741888415385299952649631
//! 57357271266846838237795794934881681514599279262561
//! 65719557637682166874879327798598143881961925499217
//! 71484759148259586125936169723614727183472583829458
//! 28178525553928963666413917477752412858886352396999
//! 57545635726865674683797678579481878968159298917926
//! 57944568656815567976792667818781377892989248891319
//! 75698651748671976285978218739618932984172914319528
//! 56475739656758684176786979528789718163989182927419
//! 67554889357866599146897761125791887223681299833479
//!
//! Equipped with the full map, you can now find a path from the top left corner to the bottom right corner with the lowest total risk:
//!
//! 11637517422274862853338597396444961841755517295286
//! 13813736722492484783351359589446246169155735727126
//! 21365113283247622439435873354154698446526571955763
//! 36949315694715142671582625378269373648937148475914
//! 74634171118574528222968563933317967414442817852555
//! 13191281372421239248353234135946434524615754563572
//! 13599124212461123532357223464346833457545794456865
//! 31254216394236532741534764385264587549637569865174
//! 12931385212314249632342535174345364628545647573965
//! 23119445813422155692453326671356443778246755488935
//! 22748628533385973964449618417555172952866628316397
//! 24924847833513595894462461691557357271266846838237
//! 32476224394358733541546984465265719557637682166874
//! 47151426715826253782693736489371484759148259586125
//! 85745282229685639333179674144428178525553928963666
//! 24212392483532341359464345246157545635726865674683
//! 24611235323572234643468334575457944568656815567976
//! 42365327415347643852645875496375698651748671976285
//! 23142496323425351743453646285456475739656758684176
//! 34221556924533266713564437782467554889357866599146
//! 33859739644496184175551729528666283163977739427418
//! 35135958944624616915573572712668468382377957949348
//! 43587335415469844652657195576376821668748793277985
//! 58262537826937364893714847591482595861259361697236
//! 96856393331796741444281785255539289636664139174777
//! 35323413594643452461575456357268656746837976785794
//! 35722346434683345754579445686568155679767926678187
//! 53476438526458754963756986517486719762859782187396
//! 34253517434536462854564757396567586841767869795287
//! 45332667135644377824675548893578665991468977611257
//! 44961841755517295286662831639777394274188841538529
//! 46246169155735727126684683823779579493488168151459
//! 54698446526571955763768216687487932779859814388196
//! 69373648937148475914825958612593616972361472718347
//! 17967414442817852555392896366641391747775241285888
//! 46434524615754563572686567468379767857948187896815
//! 46833457545794456865681556797679266781878137789298
//! 64587549637569865174867197628597821873961893298417
//! 45364628545647573965675868417678697952878971816398
//! 56443778246755488935786659914689776112579188722368
//! 55172952866628316397773942741888415385299952649631
//! 57357271266846838237795794934881681514599279262561
//! 65719557637682166874879327798598143881961925499217
//! 71484759148259586125936169723614727183472583829458
//! 28178525553928963666413917477752412858886352396999
//! 57545635726865674683797678579481878968159298917926
//! 57944568656815567976792667818781377892989248891319
//! 75698651748671976285978218739618932984172914319528
//! 56475739656758684176786979528789718163989182927419
//! 67554889357866599146897761125791887223681299833479
//!
//! The total risk of this path is 315 (the starting position is still never entered, so its risk is not counted).
//!
//! Using the full map, what is the lowest total risk of any path from the top left to the bottom right?
//!

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fmt;

fn main() {
    let map = advent_of_code_2021::input_file("input/day14.input").unwrap();
    let mut m = Maze::from_vec_string(&map);
    println!("Risk of safest path: {}", m.least_risk().unwrap());
    m.expand_map(5);
    println!(
        "Risk of multiplied safest path: {}",
        m.least_risk().unwrap()
    );
}

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Ord, PartialOrd)]
struct Pos {
    x: i32,
    y: i32,
}

impl Pos {
    pub fn new(x: i32, y: i32) -> Pos {
        Pos { x: x, y: y }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct State {
    pos: Pos,
    cost: u32,
}

impl State {
    pub fn new(pos: Pos, cost: u32) -> State {
        State {
            pos: pos,
            cost: cost,
        }
    }
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.pos.cmp(&other.pos))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Spot {
    weight: u32,
    adjacent: Vec<Pos>,
}

impl Spot {
    pub fn new(x: i32, y: i32, weight: u32) -> Spot {
        Spot {
            weight: weight,
            adjacent: vec![
                Pos::new(x - 1, y),
                Pos::new(x + 1, y),
                Pos::new(x, y - 1),
                Pos::new(x, y + 1),
            ],
        }
    }
}

struct Maze {
    map: HashMap<Pos, Spot>,
    start: Pos,
    end: Pos,
}

impl fmt::Display for Maze {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut out = String::new();

        let mut v = Vec::new();
        let mut t = Vec::new();
        t.resize((self.end.x + 1).try_into().unwrap(), '.');
        for _i in 0..self.end.y + 1 {
            v.push(t.clone());
        }

        for (pos, spot) in self.map.iter() {
            v[pos.y as usize][pos.x as usize] = char::from_digit(spot.weight, 10).unwrap();
        }

        for i in v.iter() {
            let s: String = i.into_iter().collect();
            out.push_str(&s);
            out.push('\n');
        }
        write!(f, "\n{}", out)
    }
}

impl Maze {
    pub fn from_vec_string(lines: &Vec<String>) -> Maze {
        let mut m = Maze {
            map: HashMap::new(),
            start: Pos::new(0, 0),
            end: Pos::new(0, 0),
        };
        for (y, line) in lines.iter().enumerate() {
            for (x, c) in line.chars().enumerate() {
                let w: u32 = c.to_digit(10).unwrap();
                let s = Spot::new(x.try_into().unwrap(), y.try_into().unwrap(), w);
                let pos = Pos::new(x.try_into().unwrap(), y.try_into().unwrap());
                if pos > m.end {
                    m.end = pos;
                }
                m.map.insert(pos, s);
            }
        }
        m
    }

    pub fn expand_map(&mut self, mult: u32) {
        let orig = self.map.clone();

        // Copy right.
        let x_mult: i32 = (self.end.x + 1).try_into().unwrap();
        for i in 0..mult {
            for (pos, spot) in orig.iter() {
                let mult: i32 = x_mult * i as i32;
                let newx: i32 = pos.x + mult;
                let mut neww = spot.weight + i;
                if neww > 9 {
                    neww = (neww % 10) + 1;
                }
                let s = Spot::new(newx, pos.y, neww);
                let pos = Pos::new(newx, pos.y);
                if pos > self.end {
                    self.end = pos;
                }
                self.map.insert(pos, s);
            }
        }

        let top = self.map.clone();
        // Copy down.
        let y_mult: i32 = (self.end.y + 1).try_into().unwrap();
        for i in 0..mult {
            for (pos, spot) in top.iter() {
                let mult: i32 = y_mult * i as i32;
                let newy: i32 = pos.y + mult;
                let mut neww = spot.weight + i;
                if neww > 9 {
                    neww = (neww % 10) + 1;
                }
                let s = Spot::new(pos.x, newy, neww);
                let pos = Pos::new(pos.x, newy);
                if pos > self.end {
                    self.end = pos;
                }
                self.map.insert(pos, s);
            }
        }
    }

    pub fn least_risk(&mut self) -> Option<u32> {
        let mut q = BinaryHeap::new();
        let mut dist: HashMap<Pos, u32> = self
            .map
            .iter()
            .map(|(key, _val)| (*key, u32::MAX))
            .collect();

        // Put the start in the priority queue.
        q.push(State::new(self.start, 0));
        dist.insert(self.start, 0);

        //// Explore frontier with lowest cost first.
        while let Some(c_state) = q.pop() {
            if c_state.pos == self.end {
                // Correct for adding weight on exit not entrance.
                let corrected = c_state.cost - self.map.get(&self.start).unwrap().weight
                    + self.map.get(&self.end).unwrap().weight;

                return Some(corrected);
            }

            if let Some(c_dist) = dist.get(&c_state.pos) {
                if c_state.cost > *c_dist {
                    continue;
                }
            } else {
                panic!("Failed to get current distance.")
            }

            if let Some(c_spot) = self.map.get(&c_state.pos) {
                for adj in c_spot.adjacent.iter() {
                    let n_state = State::new(*adj, c_state.cost + c_spot.weight);

                    if let Some(n_dist) = dist.get_mut(&n_state.pos) {
                        if n_state.cost < *n_dist {
                            *n_dist = n_state.cost;
                            q.push(n_state);
                        }
                    }
                }
            } else {
                panic!("Failed to get current spot.")
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pos_compare() {
        let a = Pos::new(3, 3);
        let b = Pos::new(3, 3);
        let c = Pos::new(3, 1);
        let d = Pos::new(1, 3);
        assert!(b == a);
        assert!(d < a);
        assert!(a > c);
    }

    #[test]
    fn test_state_compare() {
        let a = State::new(Pos::new(3, 3), 5);
        let b = State::new(Pos::new(3, 3), 10);
        let c = State::new(Pos::new(3, 1), 5);
        let d = State::new(Pos::new(1, 3), 5);
        // Ord is reversed for cost.
        assert!(a > b);
        assert!(a > c);
        assert!(a > d);
    }

    #[test]
    fn test_maze_from_vec_string() {
        let map = vec![
            "1163751742".to_string(),
            "1381373672".to_string(),
            "2136511328".to_string(),
            "3694931569".to_string(),
            "7463417111".to_string(),
            "1319128137".to_string(),
            "1359912421".to_string(),
            "3125421639".to_string(),
            "1293138521".to_string(),
            "2311944581".to_string(),
        ];

        let m = Maze::from_vec_string(&map);
        assert_eq!(m.map.get(&Pos::new(0, 0)).unwrap().weight, 1);
        assert_eq!(m.start, Pos::new(0, 0));
        assert_eq!(m.end, Pos::new(9, 9));
    }

    #[test]
    fn test_maze_expand_simple() {
        let map = vec!["12".to_string(), "89".to_string()];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.start, Pos::new(0, 0));
        assert_eq!(m.end, Pos::new(1, 1));

        m.expand_map(3);
        assert_eq!(m.start, Pos::new(0, 0));
        assert_eq!(m.end, Pos::new(5, 5));
        let newmap = vec![
            "122334".to_string(),
            "899112".to_string(),
            "233445".to_string(),
            "911223".to_string(),
            "344556".to_string(),
            "122334".to_string(),
        ];
        let newmaze = Maze::from_vec_string(&newmap);
        assert_eq!(m.map, newmaze.map);
    }

    #[test]
    fn test_maze_expand_full() {
        let map = vec![
            "1163751742".to_string(),
            "1381373672".to_string(),
            "2136511328".to_string(),
            "3694931569".to_string(),
            "7463417111".to_string(),
            "1319128137".to_string(),
            "1359912421".to_string(),
            "3125421639".to_string(),
            "1293138521".to_string(),
            "2311944581".to_string(),
        ];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.map.get(&Pos::new(0, 0)).unwrap().weight, 1);
        assert_eq!(m.start, Pos::new(0, 0));
        assert_eq!(m.end, Pos::new(9, 9));

        m.expand_map(5);
        assert_eq!(m.start, Pos::new(0, 0));
        assert_eq!(m.end, Pos::new(49, 49));
        let newmap = vec![
            "11637517422274862853338597396444961841755517295286".to_string(),
            "13813736722492484783351359589446246169155735727126".to_string(),
            "21365113283247622439435873354154698446526571955763".to_string(),
            "36949315694715142671582625378269373648937148475914".to_string(),
            "74634171118574528222968563933317967414442817852555".to_string(),
            "13191281372421239248353234135946434524615754563572".to_string(),
            "13599124212461123532357223464346833457545794456865".to_string(),
            "31254216394236532741534764385264587549637569865174".to_string(),
            "12931385212314249632342535174345364628545647573965".to_string(),
            "23119445813422155692453326671356443778246755488935".to_string(),
            "22748628533385973964449618417555172952866628316397".to_string(),
            "24924847833513595894462461691557357271266846838237".to_string(),
            "32476224394358733541546984465265719557637682166874".to_string(),
            "47151426715826253782693736489371484759148259586125".to_string(),
            "85745282229685639333179674144428178525553928963666".to_string(),
            "24212392483532341359464345246157545635726865674683".to_string(),
            "24611235323572234643468334575457944568656815567976".to_string(),
            "42365327415347643852645875496375698651748671976285".to_string(),
            "23142496323425351743453646285456475739656758684176".to_string(),
            "34221556924533266713564437782467554889357866599146".to_string(),
            "33859739644496184175551729528666283163977739427418".to_string(),
            "35135958944624616915573572712668468382377957949348".to_string(),
            "43587335415469844652657195576376821668748793277985".to_string(),
            "58262537826937364893714847591482595861259361697236".to_string(),
            "96856393331796741444281785255539289636664139174777".to_string(),
            "35323413594643452461575456357268656746837976785794".to_string(),
            "35722346434683345754579445686568155679767926678187".to_string(),
            "53476438526458754963756986517486719762859782187396".to_string(),
            "34253517434536462854564757396567586841767869795287".to_string(),
            "45332667135644377824675548893578665991468977611257".to_string(),
            "44961841755517295286662831639777394274188841538529".to_string(),
            "46246169155735727126684683823779579493488168151459".to_string(),
            "54698446526571955763768216687487932779859814388196".to_string(),
            "69373648937148475914825958612593616972361472718347".to_string(),
            "17967414442817852555392896366641391747775241285888".to_string(),
            "46434524615754563572686567468379767857948187896815".to_string(),
            "46833457545794456865681556797679266781878137789298".to_string(),
            "64587549637569865174867197628597821873961893298417".to_string(),
            "45364628545647573965675868417678697952878971816398".to_string(),
            "56443778246755488935786659914689776112579188722368".to_string(),
            "55172952866628316397773942741888415385299952649631".to_string(),
            "57357271266846838237795794934881681514599279262561".to_string(),
            "65719557637682166874879327798598143881961925499217".to_string(),
            "71484759148259586125936169723614727183472583829458".to_string(),
            "28178525553928963666413917477752412858886352396999".to_string(),
            "57545635726865674683797678579481878968159298917926".to_string(),
            "57944568656815567976792667818781377892989248891319".to_string(),
            "75698651748671976285978218739618932984172914319528".to_string(),
            "56475739656758684176786979528789718163989182927419".to_string(),
            "67554889357866599146897761125791887223681299833479".to_string(),
        ];
        let newmaze = Maze::from_vec_string(&newmap);
        assert_eq!(m.map, newmaze.map);
    }

    #[test]
    fn test_maze_least_risk() {
        let map = vec![
            "1163751742".to_string(),
            "1381373672".to_string(),
            "2136511328".to_string(),
            "3694931569".to_string(),
            "7463417111".to_string(),
            "1319128137".to_string(),
            "1359912421".to_string(),
            "3125421639".to_string(),
            "1293138521".to_string(),
            "2311944581".to_string(),
        ];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.least_risk(), Some(40));
    }

    #[test]
    fn test_maze_least_risk_extra1() {
        let map = vec![
            "19111".to_string(),
            "11191".to_string(),
            "99991".to_string(),
        ];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.least_risk(), Some(8));
    }

    #[test]
    fn test_maze_least_risk_extra2() {
        let map = vec![
            "19999".to_string(),
            "19111".to_string(),
            "11191".to_string(),
        ];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.least_risk(), Some(8));
    }

    #[test]
    fn test_maze_least_risk_extra3() {
        let map = vec![
            "49999".to_string(),
            "19111".to_string(),
            "11197".to_string(),
        ];

        let mut m = Maze::from_vec_string(&map);
        assert_eq!(m.least_risk(), Some(14));
    }
}
