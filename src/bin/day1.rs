//! --- Day 2: Dive! ---
//!
//! Now, you need to figure out how to pilot this thing.
//!
//! It seems like the submarine can take a series of commands like forward 1, down 2, or up 3:
//!
//!     forward X increases the horizontal position by X units.
//!     down X increases the depth by X units.
//!     up X decreases the depth by X units.
//!
//! Note that since you're on a submarine, down and up affect your depth, and so they have the opposite result of what you might expect.
//!
//! The submarine seems to already have a planned course (your puzzle input). You should probably figure out where it's going. For example:
//!
//! forward 5
//! down 5
//! forward 8
//! up 3
//! down 8
//! forward 2
//!
//! Your horizontal position and depth both start at 0. The steps above would then modify them as follows:
//!
//!     forward 5 adds 5 to your horizontal position, a total of 5.
//!     down 5 adds 5 to your depth, resulting in a value of 5.
//!     forward 8 adds 8 to your horizontal position, a total of 13.
//!     up 3 decreases your depth by 3, resulting in a value of 2.
//!     down 8 adds 8 to your depth, resulting in a value of 10.
//!     forward 2 adds 2 to your horizontal position, a total of 15.
//!
//! After following these instructions, you would have a horizontal position of 15 and a depth of 10. (Multiplying these together produces 150.)
//!
//! Calculate the horizontal position and depth you would have after following the planned course. What do you get if you multiply your final horizontal position by your final depth?
//!
//! --- Part Two ---
//! Based on your calculations, the planned course doesn't seem to make any sense. You find the submarine manual and discover that the process is actually slightly more complicated.
//!
//! In addition to horizontal position and depth, you'll also need to track a third value, aim, which also starts at 0. The commands also mean something entirely different than you first thought:
//!
//!     down X increases your aim by X units.
//!     up X decreases your aim by X units.
//!     forward X does two things:
//!         It increases your horizontal position by X units.
//!         It increases your depth by your aim multiplied by X.
//!
//! Again note that since you're on a submarine, down and up do the opposite of what you might expect: "down" means aiming in the positive direction.
//!
//! Now, the above example does something different:
//!
//!     forward 5 adds 5 to your horizontal position, a total of 5. Because your aim is 0, your depth does not change.
//!     down 5 adds 5 to your aim, resulting in a value of 5.
//!     forward 8 adds 8 to your horizontal position, a total of 13. Because your aim is 5, your depth increases by 8*5=40.
//!     up 3 decreases your aim by 3, resulting in a value of 2.
//!     down 8 adds 8 to your aim, resulting in a value of 10.
//!     forward 2 adds 2 to your horizontal position, a total of 15. Because your aim is 10, your depth increases by 2*10=20 to a total of 60.
//!
//! After following these new instructions, you would have a horizontal position of 15 and a depth of 60. (Multiplying these produces 900.)
//!
//! Using this new interpretation of the commands, calculate the horizontal position and depth you would have after following the planned course. What do you get if you multiply your final horizontal position by your final depth?
//!

use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let course = input_file_to_course("input/day1.input").unwrap();
    let mut p = Position::new();
    p.add_course(&course[..], false);
    println!("Position: {:?}", p);
    println!("Answer: {}", p.horiz * p.depth);
    let mut p = Position::new();
    p.add_course(&course[..], true);
    println!("Aim Position: {:?}", p);
    println!("Aim Answer: {}", p.horiz * p.depth);
}

#[derive(Debug)]
enum Move {
    Forward(u32),
    Down(u32),
    Up(u32),
}

#[derive(Debug)]
struct Position {
    horiz: i32,
    depth: i32,
    aim: i32,
}

impl Position {
    pub fn new() -> Position {
        Position {
            horiz: 0,
            depth: 0,
            aim: 0,
        }
    }

    pub fn add_move(&mut self, now: &Move, aim: bool) {
        match now {
            Move::Forward(x) => {
                self.horiz += *x as i32;
                if aim {
                    self.depth += self.aim * *x as i32;
                }
            }
            Move::Down(x) => {
                if aim {
                    self.aim += *x as i32;
                } else {
                    self.depth += *x as i32;
                }
            }
            Move::Up(x) => {
                if aim {
                    self.aim -= *x as i32;
                } else {
                    self.depth -= *x as i32;
                }
            }
        }
    }

    pub fn add_course(&mut self, course: &[Move], aim: bool) {
        for now in course.iter() {
            self.add_move(now, aim);
        }
    }
}

fn input_file_to_course(filename: &str) -> Result<Vec<Move>, Box<dyn Error>> {
    // Open file.
    let path = Path::new(filename);
    let file = File::open(&path)?;

    // Read each line into the vector.
    let mut inputs = Vec::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(s) = line {
            let split = s.split_whitespace().collect::<Vec<&str>>();
            match split[0] {
                "forward" => inputs.push(Move::Forward(split[1].parse()?)),
                "up" => inputs.push(Move::Up(split[1].parse()?)),
                "down" => inputs.push(Move::Down(split[1].parse()?)),
                _ => return Err("Invalid move.")?,
            }
        }
    }

    Ok(inputs)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_position_new() {
        let p = Position::new();
        assert_eq!(p.horiz, 0);
        assert_eq!(p.depth, 0);
    }

    #[test]
    fn test_position_move_forward_without_aim() {
        let mut p = Position::new();
        let m = Move::Forward(5);
        p.add_move(&m, false);
        assert_eq!(p.horiz, 5);
        assert_eq!(p.depth, 0);
    }

    #[test]
    fn test_position_move_forward_with_aim_0() {
        let mut p = Position::new();
        let m = Move::Forward(5);
        p.add_move(&m, true);
        assert_eq!(p.horiz, 5);
        assert_eq!(p.depth, 0);
    }

    #[test]
    fn test_position_move_forward_with_aim_set() {
        let mut p = Position::new();
        let m = Move::Down(5);
        p.add_move(&m, true);
        let m = Move::Forward(8);
        p.add_move(&m, true);
        assert_eq!(p.horiz, 8);
        assert_eq!(p.depth, 40);
    }

    #[test]
    fn test_position_move_down() {
        let mut p = Position::new();
        let m = Move::Down(3);
        p.add_move(&m, false);
        assert_eq!(p.horiz, 0);
        assert_eq!(p.depth, 3);
    }

    #[test]
    fn test_position_move_up() {
        let mut p = Position::new();
        let m = Move::Up(8);
        p.add_move(&m, false);
        assert_eq!(p.horiz, 0);
        assert_eq!(p.depth, -8);
    }

    #[test]
    fn test_position_course_without_aim() {
        let input = [
            Move::Forward(5),
            Move::Down(5),
            Move::Forward(8),
            Move::Up(3),
            Move::Down(8),
            Move::Forward(2),
        ];
        let mut p = Position::new();
        p.add_course(&input, false);
        assert_eq!(p.horiz, 15);
        assert_eq!(p.depth, 10);
    }

    #[test]
    fn test_position_course_with_aim() {
        let input = [
            Move::Forward(5),
            Move::Down(5),
            Move::Forward(8),
            Move::Up(3),
            Move::Down(8),
            Move::Forward(2),
        ];
        let mut p = Position::new();
        p.add_course(&input, true);
        assert_eq!(p.horiz, 15);
        assert_eq!(p.depth, 60);
    }
}
