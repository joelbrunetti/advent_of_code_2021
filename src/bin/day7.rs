//! --- Day 8: Seven Segment Search ---
//!
//! You barely reach the safety of the cave when the whale smashes into the cave mouth, collapsing it. Sensors indicate another exit to this cave at a much greater depth, so you have no choice but to press on.
//!
//! As your submarine slowly makes its way through the cave system, you notice that the four-digit seven-segment displays in your submarine are malfunctioning; they must have been damaged during the escape. You'll be in a lot of trouble without them, so you'd better figure out what's wrong.
//!
//! Each digit of a seven-segment display is rendered by turning on or off any of seven segments named a through g:
//!
//!   0:      1:      2:      3:      4:
//!  aaaa    ....    aaaa    aaaa    ....
//! b    c  .    c  .    c  .    c  b    c
//! b    c  .    c  .    c  .    c  b    c
//!  ....    ....    dddd    dddd    dddd
//! e    f  .    f  e    .  .    f  .    f
//! e    f  .    f  e    .  .    f  .    f
//!  gggg    ....    gggg    gggg    ....
//!
//!   5:      6:      7:      8:      9:
//!  aaaa    aaaa    aaaa    aaaa    aaaa
//! b    .  b    .  .    c  b    c  b    c
//! b    .  b    .  .    c  b    c  b    c
//!  dddd    dddd    ....    dddd    dddd
//! .    f  e    f  .    f  e    f  .    f
//! .    f  e    f  .    f  e    f  .    f
//!  gggg    gggg    ....    gggg    gggg
//!
//! So, to render a 1, only segments c and f would be turned on; the rest would be off. To render a 7, only segments a, c, and f would be turned on.
//!
//! The problem is that the signals which control the segments have been mixed up on each display. The submarine is still trying to display numbers by producing output on signal wires a through g, but those wires are connected to segments randomly. Worse, the wire/segment connections are mixed up separately for each four-digit display! (All of the digits within a display use the same connections, though.)
//!
//! So, you might know that only signal wires b and g are turned on, but that doesn't mean segments b and g are turned on: the only digit that uses two segments is 1, so it must mean segments c and f are meant to be on. With just that information, you still can't tell which wire (b/g) goes to which segment (c/f). For that, you'll need to collect more information.
//!
//! For each display, you watch the changing signals for a while, make a note of all ten unique signal patterns you see, and then write down a single four digit output value (your puzzle input). Using the signal patterns, you should be able to work out which pattern corresponds to which digit.
//!
//! For example, here is what you might see in a single entry in your notes:
//!
//! acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
//! cdfeb fcadb cdfeb cdbaf
//!
//! (The entry is wrapped here to two lines so it fits; in your notes, it will all be on a single line.)
//!
//! Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output value. Within an entry, the same wire/segment connections are used (but you don't know what the connections actually are). The unique signal patterns correspond to the ten different ways the submarine tries to render a digit using the current wire/segment connections. Because 7 is the only digit that uses three segments, dab in the above example means that to render a 7, signal lines d, a, and b are on. Because 4 is the only digit that uses four segments, eafb means that to render a 4, signal lines e, a, f, and b are on.
//!
//! Using this information, you should be able to work out which combination of signal wires corresponds to each of the ten digits. Then, you can decode the four digit output value. Unfortunately, in the above example, all of the digits in the output value (cdfeb fcadb cdfeb cdbaf) use five segments and are more difficult to deduce.
//!
//! For now, focus on the easy digits. Consider this larger example:
//!
//! be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
//! fdgacbe cefdb cefbgd gcbe
//! edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
//! fcgedb cgb dgebacf gc
//! fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
//! cg cg fdcagb cbg
//! fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
//! efabcd cedba gadfec cb
//! aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
//! gecf egdcabf bgf bfgea
//! fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
//! gebdcfa ecba ca fadegcb
//! dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
//! cefg dcbef fcge gbcadfe
//! bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
//! ed bcgafe cdgba cbgef
//! egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
//! gbdfcae bgc cg cgb
//! gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
//! fgae cfgab fg bagce
//!
//! Because the digits 1, 4, 7, and 8 each use a unique number of segments, you should be able to tell which combinations of signals correspond to those digits. Counting only digits in the output values (the part after | on each line), in the above example, there are 26 instances of digits that use a unique number of segments (highlighted above).
//!
//! In the output values, how many times do digits 1, 4, 7, or 8 appear?
//!
//!--- Part Two ---
//!
//! Through a little deduction, you should now be able to determine the remaining digits. Consider again the first example above:
//!
//! acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
//! cdfeb fcadb cdfeb cdbaf
//!
//! After some careful analysis, the mapping between signal wires and segments only make sense in the following configuration:
//!
//!  dddd
//! e    a
//! e    a
//!  ffff
//! g    b
//! g    b
//!  cccc
//!
//! So, the unique signal patterns would correspond to the following digits:
//!
//!     acedgfb: 8
//!     cdfbe: 5
//!     gcdfa: 2
//!     fbcad: 3
//!     dab: 7
//!     cefabd: 9
//!     cdfgeb: 6
//!     eafb: 4
//!     cagedb: 0
//!     ab: 1
//!
//! Then, the four digits of the output value can be decoded:
//!
//!     cdfeb: 5
//!     fcadb: 3
//!     cdfeb: 5
//!     cdbaf: 3
//!
//! Therefore, the output value for this entry is 5353.
//!
//! Following this same process for each entry in the second, larger example above, the output value of each entry can be determined:
//!
//!     fdgacbe cefdb cefbgd gcbe: 8394
//!     fcgedb cgb dgebacf gc: 9781
//!     cg cg fdcagb cbg: 1197
//!     efabcd cedba gadfec cb: 9361
//!     gecf egdcabf bgf bfgea: 4873
//!     gebdcfa ecba ca fadegcb: 8418
//!     cefg dcbef fcge gbcadfe: 4548
//!     ed bcgafe cdgba cbgef: 1625
//!     gbdfcae bgc cg cgb: 8717
//!     fgae cfgab fg bagce: 4315
//!
//! Adding all of the output values in this larger example produces 61229.
//!
//! For each entry, determine all of the wire/segment connections and decode the four-digit output values. What do you get if you add up all of the output values?
//!

use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let input = input_file_to_list("input/day7.input").unwrap();
    let count = find_unique(&input[..]);
    println!("Digits 1, 4, 6, 8: {}", count);
    let sum = sum_decode(&input[..]);
    println!("Decoded Sum: {}", sum);
}

fn input_file_to_list(filename: &str) -> Result<Vec<String>, Box<dyn Error>> {
    // Open file.
    let path = Path::new(filename);
    let file = File::open(&path)?;

    // Read each line into the vector.
    let mut inputs = Vec::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(x) = line {
            inputs.push(x);
        }
    }

    Ok(inputs)
}

/// Find the number of times the digits 1 (2 segments), 4 (4 segments), 7 (3 segments), & 8 (7 segments) appear after | in a string.
fn find_unique_in_line(line: &str) -> i32 {
    let mut sum = 0;
    let a = line.split("|").collect::<Vec<&str>>()[1];
    for b in a.trim().split_whitespace() {
        match b.len() {
            2 | 3 | 4 | 7 => sum += 1,
            _ => {}
        }
    }
    sum
}

fn find_unique(input: &[String]) -> i32 {
    let mut sum = 0;
    for i in input.iter() {
        sum += find_unique_in_line(i);
    }
    sum
}

///Equivilent segments. They contain the same set of letters.
fn equiv_seg(a: &str, b: &str) -> bool {
    if a.len() == b.len() {
        for c in a.chars() {
            if !b.contains(c) {
                return false;
            }
        }
        true
    } else {
        false
    }
}

/// A segment contins B segment.
fn contains_seg(a: &str, b: &str) -> bool {
    if a.len() >= b.len() {
        for c in b.chars() {
            if !a.contains(c) {
                return false;
            }
        }
        true
    } else {
        false
    }
}

/// Remove b characters from a.
fn sub_seg(a: &str, b: &str) -> String {
    let mut s = String::new();
    s.push_str(&a);
    for c in b.chars() {
        s.retain(|x| x != c);
    }
    s
}

fn find_length(line: &str, length: i32) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == length {
            return Some(n.to_string());
        }
    }
    None
}

fn find_8(line: &str) -> Option<String> {
    find_length(line, 7)
}

fn find_1(line: &str) -> Option<String> {
    find_length(line, 2)
}

fn find_7(line: &str) -> Option<String> {
    find_length(line, 3)
}

fn find_4(line: &str) -> Option<String> {
    find_length(line, 4)
}

fn find_6(line: &str, one: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == 6 {
            if !contains_seg(n, one) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn find_9(line: &str, four: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == 6 {
            if contains_seg(n, four) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn find_0(line: &str, nine: &str, six: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == 6 {
            if !equiv_seg(n, nine) && !equiv_seg(n, six) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn find_3(line: &str, one: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == 5 {
            if contains_seg(n, one) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn find_2(line: &str, one: &str, six: &str, eight: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    let top_right = sub_seg(eight, six);
    for n in nums {
        if n.len() as i32 == 5 {
            if !contains_seg(n, one) && contains_seg(n, top_right.as_str()) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn find_5(line: &str, two: &str, three: &str) -> Option<String> {
    let nums = line.split("|").collect::<Vec<&str>>()[0]
        .trim()
        .split_whitespace();
    for n in nums {
        if n.len() as i32 == 5 {
            if !equiv_seg(n, two) && !equiv_seg(n, three) {
                return Some(n.to_string());
            }
        }
    }
    None
}

fn map(line: &str, map: &mut HashMap<i32, String>) {
    map.insert(8, find_8(line).unwrap());
    map.insert(1, find_1(line).unwrap());
    map.insert(7, find_7(line).unwrap());
    map.insert(4, find_4(line).unwrap());
    map.insert(6, find_6(line, map.get(&1).unwrap()).unwrap());
    map.insert(9, find_9(line, map.get(&4).unwrap()).unwrap());
    map.insert(
        0,
        find_0(line, map.get(&9).unwrap(), map.get(&6).unwrap()).unwrap(),
    );
    map.insert(3, find_3(line, map.get(&1).unwrap()).unwrap());
    map.insert(
        2,
        find_2(
            line,
            map.get(&1).unwrap(),
            map.get(&6).unwrap(),
            map.get(&8).unwrap(),
        )
        .unwrap(),
    );
    map.insert(
        5,
        find_5(line, map.get(&2).unwrap(), map.get(&3).unwrap()).unwrap(),
    );
}

fn decode(line: &str) -> i32 {
    let mut sum = 0;
    let mut h = HashMap::new();
    map(line, &mut h);
    let nums = line.split("|").collect::<Vec<&str>>()[1]
        .trim()
        .split_whitespace();
    for (i, n) in nums.enumerate() {
        for (key, val) in h.iter() {
            if equiv_seg(n, val) {
                match i {
                    0 => sum += key * 1000,
                    1 => sum += key * 100,
                    2 => sum += key * 10,
                    3 => sum += key,
                    _ => {}
                }
                break;
            }
        }
    }
    sum
}

fn sum_decode(input: &[String]) -> i32 {
    let mut sum = 0;
    for i in input.iter() {
        sum += decode(i);
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum_decode() {
        let input: [String; 10] = [
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe".to_string(),
        "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc".to_string(),
        "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg".to_string(),
        "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb".to_string(),
        "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea".to_string(),
        "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb".to_string(),
        "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe".to_string(),
        "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef".to_string(),
        "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb".to_string(),
        "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce".to_string(),
    ];

        assert_eq!(sum_decode(&input[..]), 61229);
    }

    #[test]
    fn test_decode_extra() {
        let i = decode(
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
        );
        assert_eq!(i, 8394);
    }

    #[test]
    fn test_decode() {
        let i = decode(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
        );
        assert_eq!(i, 5353);
    }

    #[test]
    fn test_map() {
        let mut h = HashMap::new();
        map(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            &mut h,
        );
        let mut k = HashMap::new();
        k.insert(8, "acedgfb".to_string());
        k.insert(5, "cdfbe".to_string());
        k.insert(2, "gcdfa".to_string());
        k.insert(3, "fbcad".to_string());
        k.insert(7, "dab".to_string());
        k.insert(9, "cefabd".to_string());
        k.insert(6, "cdfgeb".to_string());
        k.insert(4, "eafb".to_string());
        k.insert(0, "cagedb".to_string());
        k.insert(1, "ab".to_string());
        assert_eq!(h, k);
    }

    #[test]
    fn test_find_5() {
        let l = find_5(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "gcdfa",
            "fbcad",
        )
        .unwrap();
        assert_eq!(l, "cdfbe".to_string());
    }

    #[test]
    fn test_find_2() {
        let l = find_2(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "ab",
            "cdfgeb",
            "acedgfb",
        )
        .unwrap();
        assert_eq!(l, "gcdfa".to_string());
    }

    #[test]
    fn test_find_3() {
        let l = find_3(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "ab",
        )
        .unwrap();
        assert_eq!(l, "fbcad".to_string());
    }

    #[test]
    fn test_find_0() {
        let l = find_0(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "cdfgeb",
            "cefabd",
        )
        .unwrap();
        assert_eq!(l, "cagedb".to_string());
    }

    #[test]
    fn test_find_9() {
        let l = find_9(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "eafb",
        )
        .unwrap();
        assert_eq!(l, "cefabd".to_string());
    }

    #[test]
    fn test_find_6() {
        let l = find_6(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
            "ab",
        )
        .unwrap();
        assert_eq!(l, "cdfgeb".to_string());
    }

    #[test]
    fn test_find_4() {
        let l = find_4(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
        )
        .unwrap();
        assert_eq!(l, "eafb".to_string());
    }

    #[test]
    fn test_find_7() {
        let l = find_7(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
        )
        .unwrap();
        assert_eq!(l, "dab".to_string());
    }

    #[test]
    fn test_find_1() {
        let l = find_1(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
        )
        .unwrap();
        assert_eq!(l, "ab".to_string());
    }

    #[test]
    fn test_find_8() {
        let l = find_8(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf",
        )
        .unwrap();
        assert_eq!(l, "acedgfb".to_string());
    }

    #[test]
    fn test_find_length() {
        let l = find_length(
            "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
            7,
        )
        .unwrap();
        assert_eq!(l, "aecbfdg".to_string());
    }

    #[test]
    fn test_sub_seg() {
        let a = sub_seg("afbg", "cbf");
        assert!(contains_seg(a.as_str(), "ag"));
        assert!(!contains_seg(a.as_str(), "cbf"));
    }

    #[test]
    fn test_contains_seg_true() {
        assert_eq!(contains_seg("fbg", "bf"), true);
    }

    #[test]
    fn test_contains_seg_false() {
        assert_eq!(contains_seg("fbg", "bc"), false);
    }

    #[test]
    fn test_equiv_seg_true() {
        assert_eq!(equiv_seg("fbg", "bgf"), true);
    }

    #[test]
    fn test_equiv_seg_false() {
        assert_eq!(equiv_seg("fbg", "bgc"), false);
    }

    #[test]
    fn test_find_unique_in_line() {
        assert_eq!(find_unique_in_line("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea"), 3);
    }

    #[test]
    fn test_find_unique() {
        let input: [String; 10] = [
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe".to_string(),
        "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc".to_string(),
        "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg".to_string(),
        "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb".to_string(),
        "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea".to_string(),
        "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb".to_string(),
        "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe".to_string(),
        "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef".to_string(),
        "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb".to_string(),
        "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce".to_string(),
    ];

        assert_eq!(find_unique(&input[..]), 26);
    }
}
