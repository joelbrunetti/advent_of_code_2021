//! --- Day 3: Binary Diagnostic ---
//!
//! The submarine has been making some odd creaking noises, so you ask it to produce a diagnostic report just in case.
//!
//! The diagnostic report (your puzzle input) consists of a list of binary numbers which, when decoded properly, can tell you many useful things about the conditions of the submarine. The first parameter to check is the power consumption.
//!
//! You need to use the binary numbers in the diagnostic report to generate two new binary numbers (called the gamma rate and the epsilon rate). The power consumption can then be found by multiplying the gamma rate by the epsilon rate.
//!
//! Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:
//!
//! 00100
//! 11110
//! 10110
//! 10111
//! 10101
//! 01111
//! 00111
//! 11100
//! 10000
//! 11001
//! 00010
//! 01010
//!
//! Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.
//!
//! The most common second bit of the numbers in the diagnostic report is 0, so the second bit of the gamma rate is 0.
//!
//! The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and so the final three bits of the gamma rate are 110.
//!
//! So, the gamma rate is the binary number 10110, or 22 in decimal.
//!
//! The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used. So, the epsilon rate is 01001, or 9 in decimal. Multiplying the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.
//!
//! Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate, then multiply them together. What is the power consumption of the submarine? (Be sure to represent your answer in decimal, not binary.)
//!
//! --- Part Two ---
//!
//! Next, you should verify the life support rating, which can be determined by multiplying the oxygen generator rating by the CO2 scrubber rating.
//!
//! Both the oxygen generator rating and the CO2 scrubber rating are values that can be found in your diagnostic report - finding them is the tricky part. Both values are located using a similar process that involves filtering out values until only one remains. Before searching for either rating value, start with the full list of binary numbers from your diagnostic report and consider just the first bit of those numbers. Then:
//!
//!     Keep only numbers selected by the bit criteria for the type of rating value for which you are searching. Discard numbers which do not match the bit criteria.
//!     If you only have one number left, stop; this is the rating value for which you are searching.
//!     Otherwise, repeat the process, considering the next bit to the right.
//!
//! The bit criteria depends on which type of rating value you want to find:
//!
//!     To find oxygen generator rating, determine the most common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 1 in the position being considered.
//!     To find CO2 scrubber rating, determine the least common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 0 in the position being considered.
//!
//! For example, to determine the oxygen generator rating value using the same example diagnostic report from above:
//!
//!     Start with all 12 numbers and consider only the first bit of each number. There are more 1 bits (7) than 0 bits (5), so keep only the 7 numbers with a 1 in the first position: 11110, 10110, 10111, 10101, 11100, 10000, and 11001.
//!     Then, consider the second bit of the 7 remaining numbers: there are more 0 bits (4) than 1 bits (3), so keep only the 4 numbers with a 0 in the second position: 10110, 10111, 10101, and 10000.
//!     In the third position, three of the four numbers have a 1, so keep those three: 10110, 10111, and 10101.
//!     In the fourth position, two of the three numbers have a 1, so keep those two: 10110 and 10111.
//!     In the fifth position, there are an equal number of 0 bits and 1 bits (one each). So, to find the oxygen generator rating, keep the number with a 1 in that position: 10111.
//!     As there is only one number left, stop; the oxygen generator rating is 10111, or 23 in decimal.
//!
//! Then, to determine the CO2 scrubber rating value from the same example above:
//!
//!     Start again with all 12 numbers and consider only the first bit of each number. There are fewer 0 bits (5) than 1 bits (7), so keep only the 5 numbers with a 0 in the first position: 00100, 01111, 00111, 00010, and 01010.
//!     Then, consider the second bit of the 5 remaining numbers: there are fewer 1 bits (2) than 0 bits (3), so keep only the 2 numbers with a 1 in the second position: 01111 and 01010.
//!     In the third position, there are an equal number of 0 bits and 1 bits (one each). So, to find the CO2 scrubber rating, keep the number with a 0 in that position: 01010.
//!     As there is only one number left, stop; the CO2 scrubber rating is 01010, or 10 in decimal.
//!
//! Finally, to find the life support rating, multiply the oxygen generator rating (23) by the CO2 scrubber rating (10) to get 230.
//!
//! Use the binary numbers in your diagnostic report to calculate the oxygen generator rating and CO2 scrubber rating, then multiply them together. What is the life support rating of the submarine? (Be sure to represent your answer in decimal, not binary.)
//!

use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let report = input_file_to_list("input/day2.input").unwrap();
    let b = BitSum::new(12, &report);
    println!(
        "Solution Epsilons: {}, Gamma: {}, O2: {}, CO2: {}",
        b.get_epsilon(),
        b.get_gamma(),
        b.get_o2().unwrap(),
        b.get_co2().unwrap(),
    );
    println!(
        "Answer 1: {}",
        b.get_epsilon() as u32 * b.get_gamma() as u32
    );
    println!(
        "Answer 2: {}",
        b.get_o2().unwrap() as u32 * b.get_co2().unwrap() as u32
    );
}

struct BitSum<'a> {
    depth: i32,
    report: &'a [u16],
    position: [i32; u16::BITS as usize],
    epsilon: Option<u16>,
    gamma: Option<u16>,
}

impl<'a> BitSum<'a> {
    pub fn new(depth: i32, report: &[u16]) -> BitSum {
        let mut x = BitSum {
            depth: depth,
            report: report,
            position: [0; u16::BITS as usize],
            epsilon: None,
            gamma: None,
        };

        x.sum_report();
        x
    }

    fn sum_entry(&mut self, word: u16) {
        for (i, x) in self.position.iter_mut().enumerate() {
            if word & (0b1 << i) > 0 {
                *x += 1;
            }
        }
    }

    fn sum_report(&mut self) {
        for x in self.report.iter() {
            self.sum_entry(*x);
        }
    }

    pub fn get_epsilon(&self) -> u16 {
        match self.epsilon {
            Some(e) => e,
            None => {
                let mut e: u16 = 0;
                for (i, x) in self.position.iter().enumerate() {
                    if *x > self.report.len() as i32 / 2 {
                        e += 0b1 << i;
                    }
                }
                e
            }
        }
    }

    pub fn get_gamma(&self) -> u16 {
        match self.gamma {
            Some(e) => e,
            None => !self.get_epsilon() & !(u16::MAX << self.depth),
        }
    }

    pub fn get_o2(&self) -> Option<u16> {
        self.recurse_o2(self.depth - 1, self.report.to_vec())
    }

    fn recurse_o2(&self, bit: i32, remainder: Vec<u16>) -> Option<u16> {
        if remainder.len() == 1 {
            Some(remainder[0])
        } else if bit < 0 {
            None
        } else {
            let mut ones = 0;
            let mut zeros = 0;
            for x in remainder.iter() {
                if x & (0b1 << bit) > 0 {
                    ones += 1;
                } else {
                    zeros += 1;
                }
            }
            if ones >= zeros {
                let mut new_remainder: Vec<u16> = Vec::new();
                for x in remainder.iter() {
                    if x & (0b1 << bit) > 0 {
                        new_remainder.push(*x);
                    }
                }
                self.recurse_o2(bit - 1, new_remainder)
            } else {
                let mut new_remainder: Vec<u16> = Vec::new();
                for x in remainder.iter() {
                    if x & (0b1 << bit) == 0 {
                        new_remainder.push(*x);
                    }
                }
                self.recurse_o2(bit - 1, new_remainder)
            }
        }
    }

    pub fn get_co2(&self) -> Option<u16> {
        self.recurse_co2(self.depth - 1, self.report.to_vec())
    }

    fn recurse_co2(&self, bit: i32, remainder: Vec<u16>) -> Option<u16> {
        if remainder.len() == 1 {
            Some(remainder[0])
        } else if bit < 0 {
            None
        } else {
            let mut ones = 0;
            let mut zeros = 0;
            for x in remainder.iter() {
                if x & (0b1 << bit) > 0 {
                    ones += 1;
                } else {
                    zeros += 1;
                }
            }
            if zeros > ones {
                let mut new_remainder: Vec<u16> = Vec::new();
                for x in remainder.iter() {
                    if x & (0b1 << bit) > 0 {
                        new_remainder.push(*x);
                    }
                }
                self.recurse_co2(bit - 1, new_remainder)
            } else {
                let mut new_remainder: Vec<u16> = Vec::new();
                for x in remainder.iter() {
                    if x & (0b1 << bit) == 0 {
                        new_remainder.push(*x);
                    }
                }
                self.recurse_co2(bit - 1, new_remainder)
            }
        }
    }
}

fn input_file_to_list(filename: &str) -> Result<Vec<u16>, Box<dyn Error>> {
    // Open file.
    let path = Path::new(filename);
    let file = File::open(&path)?;

    // Read each line into the vector.
    let mut inputs = Vec::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(x) = line {
            inputs.push(u16::from_str_radix(x.as_str(), 2)?);
        }
    }

    Ok(inputs)
}

#[cfg(test)]
mod tests {
    use super::*;

    const REPORT: [u16; 12] = [
        0b00100u16, 0b11110u16, 0b10110u16, 0b10111u16, 0b10101u16, 0b01111u16, 0b00111u16,
        0b11100u16, 0b10000u16, 0b11001u16, 0b00010u16, 0b01010u16,
    ];

    #[test]
    fn test_bitsum_new() {
        let b = BitSum::new(5, &[0]);
        assert_eq!(b.depth, 5);
        assert_eq!(b.position, [0; u16::BITS as usize]);
    }

    #[test]
    fn test_bitsum_sum_entry() {
        let mut b = BitSum::new(5, &[0]);
        b.sum_entry(0b00100u16);
        assert_eq!(b.position, [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
    }

    #[test]
    fn test_bitsum_sum_report() {
        let b = BitSum::new(5, &REPORT);
        assert_eq!(b.position, [5, 7, 8, 5, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
    }

    #[test]
    fn test_bitsum_epsilon() {
        let b = BitSum::new(5, &REPORT);
        assert_eq!(b.get_epsilon(), 0b10110);
    }

    #[test]
    fn test_bitsum_gamma() {
        let b = BitSum::new(5, &REPORT);
        assert_eq!(b.get_gamma(), 0b01001);
    }

    #[test]
    fn test_bitsum_recurse_o2_finish() {
        let mut b = BitSum::new(5, &[0]);
        let o2 = b.recurse_o2(5, vec![0b11110]);
        assert_eq!(o2.unwrap(), 0b11110);
    }

    #[test]
    fn test_bitsum_recurse_o2_out() {
        let mut b = BitSum::new(5, &[0]);
        let o2 = b.recurse_o2(0, vec![0b11110, 0b01110]);
        assert_eq!(o2, None);
    }

    #[test]
    fn test_bitsum_recurse_o2_remove() {
        let mut b = BitSum::new(5, &[0]);
        let o2 = b.recurse_o2(4, vec![0b11110, 0b01110]);
        assert_eq!(o2.unwrap(), 0b11110);
    }

    #[test]
    fn test_bitsum_get_o2_final() {
        let b = BitSum::new(5, &REPORT);
        assert_eq!(b.get_o2().unwrap(), 23);
    }

    #[test]
    fn test_bitsum_get_co2_final() {
        let b = BitSum::new(5, &REPORT);
        assert_eq!(b.get_co2().unwrap(), 10);
    }
}
