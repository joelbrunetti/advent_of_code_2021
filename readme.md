# Advent of Code 2021

As a personal challenge I have decided to attempt [Advent of Code 2021](https://adventofcode.com/2021/) using Rust.

Rust is a language I hadn't accomplished more than hello world in prior to taking on this challenge though I have found the [Rust Book](https://doc.rust-lang.org/stable/book/) very good.

## Usage

All of the examples given in the puzzles are implemented as unit tests. There may be additional unit tests depending on the complexity.

To run the unit tests:
``` sh
cargo test
```

Each day is implemented as its own binary with the input file stored seperately.

To run the day's solution:
``` sh
cargo run --bin day0
```
